---
title: 'Rasmussen: Bush Job Approval At 45%'
author: Ryan Underdown
layout: post
permalink: /politics/rasmussen-bush-job-approval-at-45.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Politics
---
On the day before Election Day, 45% of Americans approve of the way that George W. Bush is performing his role as President. This is the President&#8217;s highest approval rating in a little over a month. Fifty-two percent (52%) disapprove of the President&#8217;s job performance.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.rasmussenreports.com/Bush_Job_Approval.htm
 [2]: http://digg.com/politics/Rasmussen_Bush_Job_Approval_At_45