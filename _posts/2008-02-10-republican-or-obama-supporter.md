---
title: Republican or Obama Supporter?
author: Ryan Underdown
excerpt: "A bumper sticker that I recently sighted on the road from Phoenix to Las Vegas.  It's getting hard to tell which side hates Hillary more."
layout: post
permalink: /politics/republican-or-obama-supporter.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
secondary_image:
  - http://indexedcontent.com/images/lifesa.png
aktt_notify_twitter:
  - yes
categories:
  - Flickr
  - Politics
tags:
  - election2008
  - hillary
  - obama
---
<p><a href="http://farm3.static.flickr.com/2127/2180089531_751927df62.jpg"><br />
<img src="http://farm3.static.flickr.com/2127/2180089531_751927df62_m.jpg"  /></a></p>
<p>Recently sighted on the road from Phoenix to Las Vegas.</p>
