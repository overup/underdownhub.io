---
title: 'Seth Meyers to Co-Anchor `SNL&#8217;s `Update&#8217;'
author: Ryan Underdown
excerpt: |
  Seth Meyers gets the plum job of "Weekend Update" anchor next to Amy Poehler in a newly streamlined "Saturday Night Live" this season, the show's creator and executive producer, Lorne Michaels, said on Thursday.  Meyers, entering his fifth season on the late-night institution, must replace the popular Tina Fey on the fake-news anchor desk.
layout: post
permalink: /digg/seth-meyers-to-co-anchor-snls-update.php
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
aktt_notify_twitter:
  - yes
categories:
  - digg
---
Seth Meyers gets the plum job of &#8220;Weekend Update&#8221; anchor next to Amy Poehler in a newly streamlined &#8220;Saturday Night Live&#8221; this season, the show&#8217;s creator and executive producer, Lorne Michaels, said on Thursday. Meyers, entering his fifth season on the late-night institution, must replace the popular Tina Fey on the fake-news anchor desk.

<a rel="nofollow" href="http://breitbart.com/news/2006/09/21/D8K9HRLG0.html">read more</a>&nbsp;|&nbsp;<a rel="nofollow" href="http://digg.com/television/Seth_Meyers_to_Co_Anchor_SNL_s_Update">digg story</a>