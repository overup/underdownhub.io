---
title: 'Feedburner: The Easiest Way To Manage A Digg Campaign'
author: Ryan Underdown
layout: post
permalink: /digg/feedburner-the-easiest-way-to-manage-a-digg-campaign.php
DiggUrl:
  - http://digg.com/software/feedburner_the_easiest_way_to_manage_a_digg_campaign
DiggData:
  - href=http://digg.com/software/feedburner_the_easiest_way_to_manage_a_digg_campaign
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
categories:
  - digg
  - SMO
---
I hadn&#8217;t checked in to my account at <a rel="nofollow" href="http://feedburner.com">feedburner.com</a> for some time and imagine my surprise when I noticed a new feature that allows you to syndicate your <a rel="nofollow" href="http://digg.com">Digg.com</a> account&#8217;s activity (or alternately a frequent poster&#8217;s activity who you wish to bury). Under the &#8216;optimize&#8217; tab there is a link splicer option. Link splicer allows you add your recently bookmarked sites from your <a rel="nofollow" href="http://del.icio.us">del.icio.us</a>, <a rel="nofollow" href="http://furl.net/">furl</a>, bloglines, and Digg.com accounts.

![feedburner’s digg.com extensions][1]

All you need to do is add in the digg user&#8217;s account name who you wish to promote/bury and instantly you have a way to send daily email to your feed&#8217;s subscribers. Let the [Ron Paul spam][2] ensue.

Feel free to bury the articles I digg by subscribing to <a rel="nofollow" href="http://feeds.feedburner.com/ru-com">my feed</a>

 [1]: http://ryanunderdown.com/wp-content/uploads/2007/05/feedb.jpg
 [2]: http://www.petitionspot.com/petitions/buryronpaul