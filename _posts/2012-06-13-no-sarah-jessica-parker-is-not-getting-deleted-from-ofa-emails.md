---
title: No, Sarah Jessica Parker Is Not Getting Deleted From OFA Emails
author: Ryan Underdown
layout: post
permalink: /email-marketing/no-sarah-jessica-parker-is-not-getting-deleted-from-ofa-emails
categories:
  - Email Marketing
tags:
  - email marketing
  - obama
  - sarah jessica parker
  - split testing
---
Lots of people are [linking][1] to an article on [observer.com][2] noting that some fundraising emails from the Obama campaign are &#8220;deleting&#8221; mention of Sarah Jessica Parker. The piece on the New York Observer points in turn to a site set up by [propublica][3] that shows several variants of an email marketing campaign. So far they have collected 7 different variants.

This is nothing new. Marketers have been split testing email marketing messages since email&#8217;s inception. As much as I&#8217;d love to poke fun at Sarah Jessica Parker, there is simply nothing sinister going on &#8211; and the messages prominently featuring SJP may in fact be the best performing variants. In fact, if they are using a &#8220;<del datetime="2012-06-13T23:01:08+00:00">One</del>Multi-Armed Bandit&#8221; approach to split testing the fact that the emails mentioning SJP have been spotted by propublica more frequently than the other variants \*may\* show her mails are performing better. Of course the sample size in this case is of extremely limited value.

 [1]: http://pjmedia.com/instapundit/144812/
 [2]: http://observer.com/2012/06/obama-sarah-jessica-parker-fundraising-emails-06120212/
 [3]: http://projects.propublica.org/emails/mailings/sarah-jessica-has-a-message-for-you
