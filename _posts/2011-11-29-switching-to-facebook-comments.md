---
title: Switching to Facebook Comments
author: Ryan Underdown
layout: post
permalink: /wordpress/switching-to-facebook-comments.php
categories:
  - Wordpress
tags:
  - comments
  - facebook
  - Wordpress
---
I recently decided to switch to facebook comments for this blog. As a result all old comments are being discarded. Sorry for the confusion.