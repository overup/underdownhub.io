---
title: Tucson Real Estate Market In The News
author: Ryan Underdown
layout: post
permalink: /real-estate/tucson-real-estate-market-in-the-news.php
DiggUrl:
  - http://digg.com/business_finance/Tucson_Real_Estate_Market_In_The_News
DiggData:
  - href=http://digg.com/business_finance/Tucson_Real_Estate_Market_In_The_News
categories:
  - Arizona
  - Real Estate
---
From [USA Today][1]

> After five years of blockbuster home sales, 2006 delivered a reality check for homeowners in Tucson. Sales of single-family homes skidded 33% from 2005, and price appreciation last year dropped to 8%, down from nearly 27% in 2005, according to DataQuick Information Systems. 

only 8% appreciation? THE SKY IS FALLING! 

> &#8220;December was a little quiet, and 2006 was definitely a time of adjustment here,&#8221; says Martha Briggs, an agent at Long Realty. But last year&#8217;s sales figures were on par with the 2000-03 period; they &#8220;only look terrible in comparison with 2005.&#8221;

What happened to the [bubble][2]?

Dave from [Barbaralasky.com][3] has some more in depth [MLS analysis][3] for the month of December.

 [1]: http://www.usatoday.com/money/economy/housing/2007-02-06-close-tucson_x.htm?csp=15
 [2]: http://www.tucsoncitizen.com/news/business/121404d1_pittmancol
 [3]: http://www.barbaralasky.com/tucson-real-estate-blog/tucson-real-estate-news/tucson-mls-statistics-for-december-2006/