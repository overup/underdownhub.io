---
title: Realtors higher paid than Attorneys
author: Ryan Underdown
excerpt: 'David Liniger, the founder of RE/MAX, is delusional.   In recent statements made to CNET, Liniger responds to the <a href="http://www.realestatejournal.com/buysell/agentsandbrokers/20050908-blackstone.html">ongoing Department of Justice case</a> involving anti-trust charges. '
layout: post
permalink: /real-estate/realtors-higher-paid-than-attorneys
wjt_diPostTopic:
  - politics
aktt_notify_twitter:
  - yes
categories:
  - Real Estate
---
David Liniger, the founder of RE/MAX, is delusional. In recent statements made to CNET, Liniger responds to the [ongoing Department of Justice case][1] involving anti-trust charges. The DOJ states:

> &#8220;NAR&#8217;s policy prevents consumers from receiving the full benefits of competition and threatens to lock in outmoded business models and discourage discounting,&#8221; the department said.
> 
> The department contends that using the Internet to deliver real-estate services &#8220;gives Web-savvy consumers more control over their search for a home&#8221; and should lead to lower prices, as it has in other sectors such as stock trading, insurance and air travel.

The biggest tangible benefit to using a licensed real estate agent is having your property listed on the MLS, or being able to browse listings offered on the MLS. This listing on the MLS notifies other NAR licensed real estate agents of the availability of the home for sale. Currently the MLS has a stranglehold on this information and actively discourages forward minded agents from innovating new ways of disseminating this information to the public that could result in cost savings. How does the NAR accomplish this? By allowing ertain brokers to discriminate against other brokers that offer innovative web based business models from viewing their listings. According to a Department of Justice [Press Release][2], the NAR knew this policy would be &#8220;abused beyond belief&#8221;. The end result: higher costs to the consumer. How is this being evidence in the market?

> From [C|NET][3]
> 
> The Internet &#8220;has not put us out of business,&#8221; said David Liniger, founder and chairman of Re/Max International, the largest real estate agency in the United States and Canada. &#8220;It will not put us out of business.&#8221;
> 
> Liniger, who founded Re/Max in 1973 and became wealthy enough to try adventures like flying a balloon nonstop around the world, said the average annual income of a Re/Max agent was $112,000 as of 2002. Today, he said, the average income has climbed to $130,000.
> 
> &#8220;People not in the real estate business think we&#8217;re overpaid&#8211;in reality we&#8217;re not,&#8221; Liniger told an audience of hundreds of real estate brokers. &#8220;You think we&#8217;re overpaid? Let&#8217;s start investigating attorneys.&#8221; 

Apparently real estate agents are now underpaid because of their grueling two month course and deserve more compensation than an attorney which goes to schools at least 7 years. According to [Salary.com][4], the national average for an intermediate Attorney is $107,523. Mr Liniger states in this article that the average salary for a realtor &trade; has climbed to $130,000. Could these recent gains have anything at all to do with the NAR&#8217;s anti competitive practices?

 [1]: http://www.realestatejournal.com/buysell/agentsandbrokers/20050908-blackstone.html
 [2]: http://www.usdoj.gov/atr/public/press_releases/2005/211008.htm
 [3]: http://news.com.com/Real+estates+Net+turf+war/2100-1038_3-6099762.html
 [4]: http://swz.salary.com/salarywizard/layoutscripts/swzl_compresult.asp?origin=secheader&#038;statecode=&#038;state=&#038;metro=&#038;city=&#038;geo=U.S.+National+Averages&#038;jobcode=LE11000002&#038;jobtitle=Attorney+II&#038;search=&#038;narrowdesc=Legal+Services&#038;narrowcode=LE01&#038;r=salswz_swzttsbtn_psr&#038;p=050205_psr_495&#038;s=salary&#038;geocode=&#038;pagenumber=&#038;zipcode=&#038;metrocode=&#038;x=56&#038;y=12
