---
title: MySQL Monitoring Service In The Works
author: Ryan Underdown
layout: post
permalink: /oss/mysql-monitoring-service-in-the-works.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - OSS
---
Developers at open-source player MySQL are hard at work on a project code-named &#8220;Merlin&#8221;, which is designed to make it easier for customers to manage and maintain the company&#8217;s database software, a source close to MySQL says. Merlin is a server-based database monitoring and advisory service which continually scans a user&#8217;s database network for &#8230;

<a rel="nofollow" href="http://www.infoworld.com/article/06/09/29/HNmysqlmonitor_1.html?source=rss&#038;url=www.infoworld.com/article/06/09/29/HNmysqlmonitor_1.html">read more</a>&nbsp;|&nbsp;<a rel="nofollow" href="http://digg.com/software/MySQL_Monitoring_Service_In_The_Works">digg story</a>