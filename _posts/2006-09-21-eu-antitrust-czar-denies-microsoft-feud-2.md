---
title: EU Antitrust Czar Denies Microsoft Feud
author: Ryan Underdown
excerpt: |
  The European Union's antitrust chief said Tuesday she had no personal feud with Microsoft Corp. despite an ongoing legal fight between her office and the software company.  "Far from pursuing a vendetta against Microsoft, the Commission's actions are guided by the desire to create the most innovation- friendly business climate in Europe..."
layout: post
permalink: /digg/eu-antitrust-czar-denies-microsoft-feud-2.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
The European Union&#8217;s antitrust chief said Tuesday she had no personal feud with Microsoft Corp. despite an ongoing legal fight between her office and the software company. &#8220;Far from pursuing a vendetta against Microsoft, the Commission&#8217;s actions are guided by the desire to create the most innovation- friendly business climate in Europe&#8230;&#8221;

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/19/D8K7VNDG3.html
 [2]: http://digg.com/tech_news/EU_Antitrust_Czar_Denies_Microsoft_Feud