---
title: Cousteau Talks Shit About Irwin
author: Ryan Underdown
excerpt: |
  ...But, he added, Irwin would "interfere with nature, jump on animals, grab them, hold them, and have this very, very spectacular, dramatic way of presenting things. Of course, it goes very well on television. It sells, it appeals to a lot people, but I think it's very misleading. You don't touch nature ... and that's why I'm still alive."
layout: post
permalink: /digg/cousteau-talks-shit-about-irwin.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
&#8230;But, he added, Irwin would &#8220;interfere with nature, jump on animals, grab them, hold them, and have this very, very spectacular, dramatic way of presenting things. Of course, it goes very well on television. It sells, it appeals to a lot people, but I think it&#8217;s very misleading. You don&#8217;t touch nature &#8230; and that&#8217;s why I&#8217;m still alive.&#8221;

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/19/D8K88DO80.html
 [2]: http://digg.com/environment/Cousteau_Talks_Shit_About_Irwin