---
title: Freddy Sanchez Takes NL Batting Title
author: Ryan Underdown
layout: post
permalink: /digg/freddy-sanchez-takes-nl-batting-title.php
categories:
  - digg
---
Sanchez was a 28-year-old utility infielder beginning only his second full season in the majors and didn&#8217;t become a starter until early May, yet hit .344 &#8211; the Pirates&#8217; highest average since Roberto Clemente&#8217;s .345 in 1969. Sanchez&#8217;s 200 hits were the second-most by a Pirates player, to Jack Wilson&#8217;s 201 in 2004, since Dave Parker had 215 in 1978.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/na/cp_m100169A.xml.html
 [2]: http://digg.com/baseball/Freddy_Sanchez_Takes_NL_Batting_Title