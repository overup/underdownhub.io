---
title: Desperate to boost your credit?
author: Ryan Underdown
layout: post
permalink: /real-estate/desperate-to-boost-your-credit.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Business
  - Finance
  - Loan Programs
  - Real Estate
---
One of my <a rel="nofollow" href="http://digg.com/business_finance/Got_Bad_Credit_Rent_out_someone_elses_Piggybacking_your_credit_score">Digg.com</a> friends [Chris][1] just passed along an interesting story on <a rel="nofollow" href="http://fe18.news.sp1.yahoo.com/s/ap/20070603/ap_on_bi_ge/cash_for_credit">yahoo</a> about some enterprising credit repair providers. They use a technique that many mortgage brokers/Loan Officers have known about for years called piggybacking.

> Instead of spending several years repairing his credit rating, which he said was marred by two forgotten cell phone bills and identity theft, the 37-year-old real estate agent paid $1,800 to an Internet-based company to bump up his score almost overnight.
> 
> <a rel="nofollow" href="http://Instantcreditbuilders.com">Instantcreditbuilders.com</a>, or ICB, helped Estruch boost his score by arranging for him to be added as an authorized user on several credit cards of people with stellar credit who were paid to allow this coattailing. Parents also use this practice when they add their children to their credit cards to help them build solid credit.
> 
> The pitch to those who are essentially renting their credit history for pay is seductive: You don&#8217;t need to worry about users of this service receiving duplicate copies of your credit cards, account numbers or any of your personal information. It&#8217;s essentially free money, they are told.

In the past if a borrower needed a few points extra to qualify for a loan program loan officers would ask them if any of their family members had an account they could get authorized on. Once the borrower is authorized as a user on the account, they do a rapid rescore and 75% of the time the prior history would be attached to the newly authorized user. This company however is taking it to a new level.

 [1]: http://digg.com/users/spinchange/profile