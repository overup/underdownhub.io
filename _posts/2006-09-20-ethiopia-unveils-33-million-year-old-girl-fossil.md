---
title: Ethiopia unveils 3.3 million-year-old girl fossil
author: Ryan Underdown
excerpt: 'Ethiopian scientists unveiled on Wednesday a 3.3 million-year-old fossil of a girl, which they believe is the most complete skeleton ever found.  The fossil including an entire skull, torso, shoulder blade and various limbs was discovered at Dikaka, some 400 kms northeast of the capital Addis Ababa near the Awash river in the Rift Valley.'
layout: post
permalink: /digg/ethiopia-unveils-33-million-year-old-girl-fossil.php
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
aktt_notify_twitter:
  - yes
categories:
  - digg
---
Ethiopian scientists unveiled on Wednesday a 3.3 million-year-old fossil of a girl, which they believe is the most complete skeleton ever found. The fossil including an entire skull, torso, shoulder blade and various limbs was discovered at Dikaka, some 400 kms northeast of the capital Addis Ababa near the Awash river in the Rift Valley.

<a rel="nofollow" href="http://today.reuters.com/news/articlenews.aspx?type=scienceNews&#038;storyid=2006-09-20T164238Z_01_L20291595_RTRUKOC_0_US-ETHIOPIA-FOSSIL.xml&#038;src=rss&#038;rpc=22">read more</a>&nbsp;|&nbsp;<a href="http://digg.com/general_sciences/Ethiopia_unveils_3_3_million_year_old_girl_fossil" rel="nofollow">digg story</a>