---
title: 'Swivel: Data 2.0 vs Digg Users'
author: Ryan Underdown
layout: post
permalink: /digg/swivel-data-20-vs-digg-users.php
DiggUrl:
  - http://digg.com/tech_news/swivel_data_2_0_vs_digg_users
DiggData:
  - href=http://digg.com/tech_news/swivel_data_2_0_vs_digg_users
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - digg
  - Ruby On Rails
---
While checking out Ruby on Rails this week I came across a cool website @ [swivel.com][1]. I am an econ buff and the ways in which you can compare data are both amazing and easy, enabling me to waste several hours that I should have been working. After perusing the usual <a rel="nofollow" href="http://www.swivel.com/graphs/show/1016984">high correlation</a> &#8211; <a rel="nofollow" href="http://swivel.com/graphs/show/5152718">zero causation</a> I grabbed a fresh copy of [Chris Finke&#8217;s][2] compiled data on the top 1000 users at Digg, conveniently formatted in CSV for easy upload to swivel. There were some interesting results. I noticed some <a rel="nofollow"href="http://www.swivel.com/data_columns/show/3364449">users I&#8217;d never heard of</a> that had dugg quite a few stories and another that had submitted over 5,000 stories only to have 7 hit the front page. The perseverance award goes to <a rel="nofollow"href="http://www.swivel.com/data_columns/show/3364523">rodtrent</a>.

Of course it should come as no surprise that the most popular digg user remains [Kevin Rose][3].

[<img alt="Profile Views by Username" src="http://www.swivel.com/graphs/image/17632242" style="border: solid 1px #rgb(0.6,0.6,0.6);" title="Click to play with this data at Swivel" />][4]

A few other items of interest for digg users can be found [here][5] and [here][6]. Overall I think swivel is a great tool. My one complaint is that there is no way to make datasets private (and selectively shareable). If you could I would definitely use it for work.

 [1]: http://swivel.com
 [2]: http://www.efinke.com/2007/06/14/top-1000-diggers-20070613/
 [3]: http://kevinrose.com/
 [4]: http://www.swivel.com/graphs/show/17632242
 [5]: http://swivel.com/graphs/show/17631738
 [6]: http://swivel.com/graphs/show/17631740