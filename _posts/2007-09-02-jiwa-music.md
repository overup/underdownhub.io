---
title: Jiwa Music
author: Ryan Underdown
layout: post
permalink: /music/jiwa-music.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - Music
tags:
  - jiwa
  - last.fm
  - songbird
---
I&#8217;ve signed up for just about every music site that exists over the last couple of years and been disappointed with all of them. I&#8217;ve ditched allofmp3.com (who I hear <a rel="nofollow" href="http://blogs.allofmp3.ru/music_news/2007/08/31/the-service-will-be-resumed/">might be coming online again</a> soon) for [pandora][1], for [last.fm][2] and most recently Last.fm in favor of [JiwaMusic][3]. (My malaysian friends tell me Jiwa means soul) To me the decision was obvious; Jiwa lets you stream full length songs on demand through your browser, Last.fm doesn&#8217;t. In addition, jiwa lets users create playlists and then embed a widget in their blog, myspace, or whatever pre-loaded with those songs to share with your friends and site visitors. Here is mine:

<table class="jiwadget" cellpadding="0" cellspacing="0" border="0" style="width:260px;">
  <tr class="jiwaHead">
    <td>
      <a title="trip hop" href="http://www.jiwa.fm/" target="_blank" style="display:block;overflow:hidden;height:39px;width:260px;background:url(http://www.jiwa.fm/res/img/embed/header.png) no-repeat scroll right;text-decoration:none;border:0;"></a>
    </td>
  </tr>
  
  <tr class="jiwaEmbed">
    <td>
    </td>
  </tr>
  
  <tr class="jiwaFoot">
    <td style="background:url(http://www.jiwa.fm/res/img/embed/bg.png) repeat-x 0 0;">
      <table cellspacing="0" cellpadding="0" border="0" style="width:260px;">
        <tr>
          <td class="jiwaView" style="width:213px;">
            <a href="http://www.jiwa.fm/#p=playlist/8336" target="_blank" style="line-height:40px;padding-left:15px;display:block;overflow:hidden;width:198px;height:40px;text-decoration:none;border:0;">trip hop</a>
          </td>
          
          <td class="jiwaPopup" style="width:47px;">
            <a href="http://www.jiwa.fm/playlist/jiwadget/popup?playlistId=8336&#038;playlistName=trip+hop&#038;ownerId=4540&#038;ownerName=elebrio" title="Ouvrir dans une fen?tre pop-up" target="_blank" style="display:block;overflow:hidden;width:47px;height:40px;background:url(http://www.jiwa.fm/res/img/embed/popup.png) no-repeat 0 0;text-decoration:none;border:0;" onclick="window.open(this.href + '&resize=0','jiwa_popup','height=380,width=234,resizable=yes,scrollbars=yes'); return false;"></a>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

Please don&#8217;t laugh at my taste in music. 

I still advocate using [songbird][4] to download, but JiwaMusic has a lot of nice functionality. Now if someone would build a script so you could search for songs to stream from Jiwa through songbird I would be in music heaven.

 [1]: http://pandora.com/
 [2]: http://last.fm
 [3]: http://www.jiwamusic.com/jiwer/elebrio
 [4]: http://sonbgirdnest.com