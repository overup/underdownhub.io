---
title: The Richest Members of Congress
author: Ryan Underdown
layout: post
permalink: /politics/the-richest-members-of-congress.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Politics
---
1 Herb Kohl (D-Wis) $219,098,029  
2 Jane Harman (D-Calif) $168,651,649  
3 John Kerry (D-Mass) $165,741,511  
4 Darrell Issa (R-Calif) $135,862,098  
5 Jay Rockefeller (D-WVa) $78,150,023 

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://opensecrets.org/pfds/overview.asp?type=W&#038;cycle=2005&#038;filter=C
 [2]: http://digg.com/politics/The_Richest_Members_of_Congress