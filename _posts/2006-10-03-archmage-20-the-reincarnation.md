---
title: 'Archmage 2.0: The Reincarnation'
author: Ryan Underdown
layout: post
permalink: /digg/archmage-20-the-reincarnation.php
categories:
  - digg
---
The Reincarnation is a massive multiplayer online game that you can play for free. In this game you take up the role of an ancient mage, guiding your fantasy kingdom with the power of your magic, the strength of your armies and your talent of managing your land.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://the-reincarnation.com/
 [2]: http://digg.com/playable_web_games/Archmage_2_0_The_Reincarnation