---
title: 'Responsive Ads with Adsense'
author: Ryan Underdown
layout: post
permalink: /adsense/responsive-ads-with-adsense
excerpt: Hack to get around the lack of support for responsive design with adsense ads
categories:
  - adsense
tags:
  - adsense
  - adwords
  - javascript
  - jquery
  - responsive web design
  - script
---
  
<h5>Google on the one hand is pushing webmasters to switch to responsive design, and on the other ignoring adsense users that use responsive design.</h5>


## UPDATE

<p>Google recently implemented a responsive ad block that essentially performs some media queries and returns the appropriate ad unit depending on screen size. My sample code block looks like:</p>
<p><script src="https://gist.github.com/underdown/6142584.js"></script></p>



### Original Post

<p>I recently switched to a &#8220;responsive&#8221; web design and couldn&#8217;t find a good way to dynamically show the proper adwords ad format depending on the viewport of the visitor&#8217;s device. This snippet uses jquery to determine viewport width and then sets the appropriate ad channel variables for show_ads.js:</p>
<p><script src="https://gist.github.com/2474608.js?file=gistfile1.js"></script></p>


<p>**UPDATE:** Today (May 23 2013) Google announced its now ok to modify the adsense code like I mentioned above (Woops! didn&#8217;t know I was breaking TOS)</p>

<p>You can read [their post announcing the changes here][1]. The relevant code snippets they provide for sites using responsive web designs looks pretty much identical to mine (minus the pointless reliance on jquery):</p>

<p><script src="https://gist.github.com/underdown/5645409.js"></script></p>

<p>More details can be found on the help pages for [adsense advertisers][2].</p>

 [1]: http://adsense.blogspot.com/2013/05/updates-to-our-modifying-ad-code-policy.html
 [2]: http://support.google.com/adsense/answer/1354736
