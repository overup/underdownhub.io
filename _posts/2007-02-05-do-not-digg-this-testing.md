---
title: 'do not digg this &#8230; testing'
author: Ryan Underdown
layout: post
permalink: /digg/do-not-digg-this-testing.php
DiggUrl:
  - http://digg.com/other_sports/do_not_digg_this_testing
DiggData:
  - href=http://digg.com/other_sports/do_not_digg_this_testing
categories:
  - digg
---
So I have been trying to get the [Gregarious][1] Word Press Plugin to work with my blog and have come across a few minor problems. The biggest is none of my old front page submissions will show up. At first I got nothing from [digg][2] at all but eventually I was able to get this error:

> Digg.com Digg This Button Error\n\nThe code implemented by the creator of this page has the following problem:\n\nA Digg Permalink URL was detected; however, that item cannot be found on the website. Please check the URL provided.\n\nVisit http://digg.com/tools/integrate for more help in debugging.

So anyway I made this post to check if it was just old posts that wouldn&#8217;t show up and indeed that is the case as you can tell by the digg badge showing up at the bottom of this article.

 [1]: http://dev.lipidity.com/feature/wp-plugin-gregarious
 [2]: http://digg.com