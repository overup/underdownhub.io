---
title: 'Homeownership &#8211; The Road to Serfdom?'
author: Ryan Underdown
excerpt: 'I came across an interesting article today by <a href="http://www.michael-hudson.com/articles/debt/Hudson,RoadToSerfdom.pdf#search=%22%22the%20new%20road%20to%20serfdom%22%20hudson%22">Michael Hudson - Distinguished Professor of Economics at the University of Missouri-Kansas City</a>.  After reading it I thought I had never read anything that was so completely off the mark.'
layout: post
permalink: /real-estate/homeownership-the-road-to-serfdom.php
aktt_notify_twitter:
  - yes
categories:
  - Finance
  - Real Estate
---
I came across an interesting article today by !PDF Warning![Michael Hudson &#8211; Distinguished Professor of Economics at the University of Missouriâ€“Kansas City][1]. After reading it I thought I had never read anything that was so completely off the mark.

From the Article:

> The reality is that, although home ownership may be a wise choice for many people, this particular real estate bubble has been carefully engineered to lure home buyers into circumstances detrimental to their own best interests. The bait is easy money. The trap is a modern equivalent to peonage, a lifetime spent working to pay off debt on an asset of rapidly dwindling value.

This paragraph makes two faulty assumptions. First and most egregious is the idea that there was a master plan set forth by the Fed to enslave Americans to mortgage repayments. I&#8217;m not a big fan of Owner-Occupied housing, as i see it as a liability and not an asset, however the flip side of paying rent and claiming no interest deduction and no possibility of equity gains is even more patently absurd. The second is that these new homeowners will never be able to pay off their loans. We are in a period of extremely low interest rates. Even now when the average loan is running 6.5% for 30 year fixed paper (and falling I might add). How long ago was it when we thought a 7.5% fixed interest rate was reasonable? This means of course that as a percentage of payments a buyer is paying less on interest expense than at any time in recent history. That one percent drop in interest rate just negated a 10% rise in home values. Lets not forget either that a god number of the homes sold during the recent boom were locked in at 5.5% or lower. A full 20% increase in buying power when compared to pre-boom financing.

I also find it hard to take the author seriously when he compares homeowners to the sharecroppers of the south at the turn of the century:

> Debtors were medieval peons or Indians bonded to Spanish plantations or the sharecropping children of slaves in the postbellum South. Few Americans today would volunteer for such an arrangement, and therefore would-be lords and barons have been forced to develop more sophisticated enticements.

Yes! That enticement is called lower interest rates!

 [1]: http://www.michael-hudson.com/articles/debt/Hudson,RoadToSerfdom.pdf#search=%22%22the%20new%20road%20to%20serfdom%22%20hudson%22