---
title: Mass. judge allows out-of-state gay couple to wed
author: Ryan Underdown
excerpt: A Massachusetts Superior Court judge ruled on Friday that a lesbian couple from Rhode Island could marry in Massachusetts because Rhode Island does not have a law specifically banning it.
layout: post
permalink: /digg/mass-judge-allows-out-of-state-gay-couple-to-wed.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
A Massachusetts Superior Court judge ruled on Friday that a lesbian couple from Rhode Island could marry in Massachusetts because Rhode Island does not have a law specifically banning it.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://today.reuters.com/news/articlenews.aspx?type=domesticNews&#038;storyid=2006-09-29T154144Z_01_N29217990_RTRUKOC_0_US-RIGHTS-GAYS-MASSACHUSETTS.xml&#038;src=rss&#038;rpc=22
 [2]: http://digg.com/politics/Mass_judge_allows_out_of_state_gay_couple_to_wed