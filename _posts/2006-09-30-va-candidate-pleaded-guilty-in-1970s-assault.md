---
title: Va. Candidate Pleaded Guilty in 1970s Assault
author: Ryan Underdown
layout: post
permalink: /digg/va-candidate-pleaded-guilty-in-1970s-assault.php
categories:
  - digg
---
Democratic congressional candidate Phil Kellam said Friday he regrets his actions in an incident that led to him pleading guilty to assaulting a woman 28 years ago, when he was a 21-year-old college student in North Carolina. Kellam, who is challenging first-term Republican Rep. Thelma Drake, said he never touched the woman but lost his temper&#8230;

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/29/D8KET85G0.html
 [2]: http://digg.com/politics/Va_Candidate_Pleaded_Guilty_in_1970s_Assault