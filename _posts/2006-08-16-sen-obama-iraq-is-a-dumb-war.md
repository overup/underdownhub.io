---
title: 'Sen. Obama: Iraq is a dumb war'
author: Ryan Underdown
excerpt: |
  |
    During a question-and-answer session, Obama pointed to many problems he perceives with the conduct of the war in Iraq.  Sometimes war is a necessary option, he said. But Iraq is, "A dumb war. We haven't thought it through," Obama said.
layout: post
permalink: /politics/sen-obama-iraq-is-a-dumb-war.php
wjt_diPostTopic:
  - politics
aktt_notify_twitter:
  - yes
categories:
  - Politics
---
> JUNCTION &#8211; Sen. Barack Obama combined sharp criticism of the Bush administration and Congress with thoughts about his own priorities at a town hall-type appearance at Gallatin County schools Monday.
> 
> During a question-and-answer session, Obama pointed to many problems he perceives with the conduct of the war in Iraq. Sometimes war is a necessary option, he said. But Iraq is, &#8220;A dumb war. We haven&#8217;t thought it through,&#8221; Obama said.

In other news Ted Kennedy called President Bush a &#8220;poopyhead&#8221;.

  
  
[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://dailyregister.com/articles/2006/08/15/news/news3.txt
 [2]: http://digg.com/political_opinion/Sen_Obama_Iraq_is_a_dumb_war