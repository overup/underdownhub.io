---
title: SEO Friendly Split Testing
author: Ryan Underdown
excerpt: "I've recently been spending most of my time split testing the designs across many of the sites I manage.  The question is always raised - how will this effect my seo?"
layout: post
permalink: /seo/seo-friendly-split-testing
categories:
  - SEO
tags:
  - canonical
  - SEO
  - split testing
---
I&#8217;ve recently been spending most of my time split testing the designs across many of the sites I manage. The question is always raised &#8211; how will this effect my seo? Unfortunately website owners aren&#8217;t allowed to serve up different page versions to search engines than to regular visitors &#8211; without risking the wrath of Google. (That is unless you&#8217;re one of the big brands) This technique is commonly referred to as &#8220;cloaking&#8221; and is expressly in [violation of Google&#8217;s webmaster guidelines][1]. 

Luckily we have a new tool at our disposal to set things straight with some level of reliability &#8211; the [link canonical][2] tag. According to Google:

> If your site has identical or vastly similar content that&#8217;s accessible through multiple URLs, this format provides you with more control over the URL returned in search results. It also helps to make sure that properties such as link popularity are consolidated to your preferred version.

Let&#8217;s assume three split test variant pages:

*   index.php
*   indexb.php
*   indexc.php

To properly point each page to the original you would simply place this tag in the HEAD statement of each page:

<pre class="brush: xml; title: ; notranslate" title="">&lt;link rel="canonical" href="http://www.domain.com/index.php"&gt;</pre>

What about other search engines you say? Well luckily [Yahoo!][3] and [Bing][4] announced support for the link canonical tag at the same time as Google. YMMV with these second-tier search engines &#8211; but at least they have stated their intent to honor it. 

A second level of protection can be added by adding a meta tag to each page to prevent indexation. I would recommend a setting of NOINDEX, FOLLOW as seen below:

<pre class="brush: xml; title: ; notranslate" title="">&lt;meta name="robots" content="NOINDEX, FOLLOW" &gt;</pre>

Obviously this method is ONLY going to work if you are split testing and not multivariate testing.

 [1]: http://www.google.com/support/webmasters/bin/answer.py?hl=en&#038;answer=66355
 [2]: http://googlewebmastercentral.blogspot.com/2009/02/specify-your-canonical.html
 [3]: http://www.ysearchblog.com/2009/02/12/fighting-duplication-adding-more-arrows-to-your-quiver/
 [4]: http://www.bing.com/community/blogs/webmaster/archive/2009/02/12/partnering-to-help-solve-duplicate-content-issues.aspx
