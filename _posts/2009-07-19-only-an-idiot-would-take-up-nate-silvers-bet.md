---
title: Only An Idiot Would Take Up Nate Silver’s Bet
author: Ryan Underdown
excerpt: Nate Silver issues a completely pointless challenge to global warming skeptics that only a fool would take him up on.
layout: post
permalink: /politics/only-an-idiot-would-take-up-nate-silvers-bet.php
aktt_notify_twitter:
  - yes
aktt_tweeted:
  - 1
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Politics
tags:
  - climate change
  - global warming
  - nate silver
  - statistics
---
Yesterday left leaning blogger Nate Silver <a rel="nofollow" href="http://www.fivethirtyeight.com/2009/07/challenge-to-climate-change-skeptics.html">issued a bet</a> to <del datetime="2009-07-19T17:07:39+00:00">global warming</del> climate change skeptics in response to John Hinderaker&#8217;s post about the [unseasonably cool summer][1] much of the country is experiencing. Silver states:

> Therefore, because I&#8217;d like to see more accountability on all sides of this debate and because I&#8217;m tired of people who don&#8217;t understand statistics and because I&#8217;d like to make some money, I issue the following challenge. 

Hello? Illegal gambling? Silver is undaunted:

> For each day that the high temperature in your hometown is at least 1 degree Fahrenheit above average, as listed by Weather Underground, you owe me $25. For each day that it is at least 1 degree Fahrenheit below average, I owe you $25.

So on one hand Silver is upset because climate skeptics (according to him) don&#8217;t understand statistics and the concept of anecdotal evidence &#8211; and then proceeds to make a bet over short term temperatures &#8211; the exact anecdotal evidence he is railing about. Regardless of the outcome you can be sure of one thing: This bet will not dissuade true believers, who will brush it off as anecdotal evidence if they lose and claim an urgent need to act if they win. You can not bet against a group of people who move the goalposts (global warming->climate change) when data doesn&#8217;t conform to their belief system. As a result you would have to be an idiot to bet Nate Silver.

UPDATE &#8211; Its worth mentioning that the average temperature over the date range Silver provided (June 21-July 18) is [70.5 &deg; F][2] while historically it is 71.96 &deg; F according to [data from weather.com][3].

 [1]: http://www.powerlineblog.com/archives/2009/07/024075.php
 [2]: http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=KMNMINNE17&#038;graphspan=custom&#038;month=7&#038;day=21&#038;year=2009&#038;monthend=7&#038;dayend=18&#038;yearend=2009
 [3]: http://www.weather.com/outlook/events/weddings/wxclimatology/daily/USMN0503?climoMonth=7