---
title: 'HOWTO: Use Songbird, Tor, and Privoxy to Anonymously Stream (or Download) Media Files'
author: Ryan Underdown
layout: post
permalink: /music/howto-use-songbird-tor-and-privoxy-to-anonymously-stream-or-download-media-files.php
DiggClick:
  - http://digg.com/software/HOWTO_Songbird_Tor_Privoxy_Anonymous_Downloading
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Music
  - OSS
---
Disclaimer: downloading media that you don&#8217;t own is against the law

<a rel="nofollow" href="http://songbirdnest.com/partners"><img src="http://songbirdnest.com/files/images/button_headphones.png" alt="Get Songbird" border="0" /></a>[<img src="http://songbirdnest.com/files/images/button_eggbag.png" border="0" alt="Get Songbird" />][1]<a rel="nofollow" href="http://songbirdnest.com/partners"><img src="http://songbirdnest.com/files/images/button_guitar.png" border="0" alt="Get Songbird" /></a><a rel="nofollow" href="http://songbirdnest.com/partners"><img src="http://songbirdnest.com/files/images/button_mac.png" border="0" alt="Get Songbird" /></a>[<img src="http://songbirdnest.com/files/images/button_poot.png" border="0" alt="Get Songbird" />][1]<a rel="nofollow" href="http://songbirdnest.com/partners"><img src="http://songbirdnest.com/files/images/button_pickup.png" border="0" alt="Get Songbird" /></a><a rel="nofollow" href="http://tor.eff.org" title="tor"><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/tor1.png' alt='tor' /></a>

In case you haven&#8217;t heard <a rel="nofollow" href="http://www.digitalmediaminute.com/article/2032/songbird-is-now-available">Songbird</a> is a great open source media organizer and content scraper. With a couple of small tweaks you can set it up to anonymously download (backups of your existing) media or stream media from millions of sources online. With a [Linux, Mac, and PC][2] compatible version (Ubuntu users check out this cool[ installer script][3]), Songbird gives you the option to store a copy of media you find through it&#8217;s various <a rel="nofollow" href="http://addons.songbirdnest.com/">search engine plugins</a> to your media collection, or bookmark it&#8217;s location online and add its URL for streaming to it&#8217;s handy built-in media library. Since Songbird is open source, there are lots of user contributed plugins to allow you to stream music from lots of online sources like last.fm, skreemr, google search, and more every day. 

### Step 1: Songbird

Install a nightly build of Songbird available <a rel="nofollow" href="http://publicsvn.songbirdnest.com/wiki/Nightly_Builds">here</a>

### Step 2: Tor &#038; Privoxy

Install the Vidalia Bundle available [here][4]. If you are running windows you will see two new icons in your icon tray letting you know that tor &#038; privoxy are running.

![icontray.png][5]

### Step 3: Tor Button (optional)

Install the Tor Button for Firefox available <a rel="nofollow" href="https://addons.mozilla.org/en-US/firefox/addon/2275">here</a>

Currently the biggest drawback from using Tor as an anonymous proxy is the download speed bottleneck using it creates. Installing the Tor Button allows you to quickly switch your proxy settings in Firefox so you can stop using Tor and Privoxy when the speed of normal browsing becomes an issue. Unfortunately, the built in version of Firefox is incompatible with the latest tor button so you will have to manually edit your proxy settings in Songbird to use a tor router. If someone knows of a way to force compatibility please let me know.

### Step 4: Manually configure Privoxy proxy settings in Songbird

Click on Tools &#8211;> Options

![Songbird Options][6]

On the Firefox dialog box that pops up click on connection settings.

![Connection Settings][7]

Enter in Privoxy&#8217;s default proxy settings (Unless you have manually changed them)

![Configure Firefox to use privoxy][8]

### Step 5: Install the skreemr search addon

Skreemr is a pretty good site to search for music. You can download the addon so you can stream music from skreemr <a rel="nofollow" href="http://addons.songbirdnest.com/extensions/detail/41">here</a>. Once installed you will need to do a quick restart of Songbird (remember songbird is firefox based and sometimes those annoying restarts are necessary) and you can start searching for music to listen to.

### Step 6: Search for music

On the top right hand corner of the Songbird GUI will you see a search box that lets you pick your search engine, again, much the same way firefox lets you pick the search engine to use. Choose skreemr and you are ready to go!

<a rel="nofollow" href='http://skreemr.com' title='skreemr.com'><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/skreemr.png' alt='skreemr search' /></a>

That&#8217;s it! Happy searching!

 [1]: http://songbirdnest.com/partners
 [2]: http://publicsvn.songbirdnest.com/wiki/Nightly_Builds
 [3]: http://www.psychocats.net/ubuntu/songbird
 [4]: http://tor.eff.org/download.html.en
 [5]: http://ryanunderdown.com/wp-content/uploads/2007/09/icontray.png
 [6]: http://ryanunderdown.com/wp-content/uploads/2007/09/songoptions.png
 [7]: http://ryanunderdown.com/wp-content/uploads/2007/09/connection.png
 [8]: http://ryanunderdown.com/wp-content/uploads/2007/09/proxysettings.png