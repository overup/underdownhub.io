---
title: Homeowners rush into fixed-rate mortgages
author: Ryan Underdown
excerpt: 'Yesterday I wrote about <a href="http://ryanunderdown.com/2006/08/22/mortgage-rates-drop-for-4th-consecutive-week/">interest rates for 30 year fixed mortgages dropping for four weeks</a> in a row.  Lo and behold the public has noticed.'
layout: post
permalink: /real-estate/homeowners-rush-into-fixed-rate-mortgages.php
aktt_notify_twitter:
  - yes
categories:
  - Finance
  - Loan Programs
  - Real Estate
---
Yesterday I wrote about [interest rates for 30 year fixed mortgages dropping for four weeks][1] in a row. Lo and behold the public has noticed:

<blockquote cite="http://custom.marketwatch.com/custom/myway-com/news-story.asp?guid={CE56B7DC-37FF-471E-B69A-7DB58A308BB6}">
  <p>
    U.S. homeowners continue to rush to refinance their mortgages, as interest rates have fallen back to a five-month low, data from the Mortgage Bankers Asssociation showed Wednesday.
  </p>
  
  <p>
    The number of applications for mortgages filed at major U.S. banks rose 0.1% last week on the increase in refinancings, the industry group said.
  </p>
</blockquote>

It&#8217;s a modest gain to be sure but expect more gains as this is a lagging indicator. The yield on the ten year held steady today at 4.81 and all signs point to no big changes tomorrow. If Bernanke listens to the [Asian markets][2], the trend may continue.

 [1]: http://ryanunderdown.com/2006/08/22/mortgage-rates-drop-for-4th-consecutive-week/
 [2]: http://today.reuters.com/news/articleinvesting.aspx?type=bondsNews&#038;storyID=2006-08-21T033558Z_01_T252568_RTRIDST_0_MARKETS-TREASURIES-ASIA.XML