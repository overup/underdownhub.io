---
title: Track IP Addresses In Google Analytics
author: Ryan Underdown
layout: post
permalink: /analytics/track-ip-addresses-in-google-analytics.php
categories:
  - Analytics
tags:
  - analytics
  - Google
  - ip address
  - tracking
---
**UPDATE: Apparently this is against Analytic&#8217;s [TOS][1]. So if you are thinking about collecting this data it might be wise NOT to.**

Now that Google Analytics allows multiple custom variables it is much easier to track your users ip addresses. Simply insert the following code in your analytics tag:

<pre class="jush">pagetracker._setCustomVar(1, "IP", "&lt;?php echo $_SERVER['REMOTE_ADDR']; ?&gt;", 2);
</pre>

To break down each variable:

*   SLOT &#8211; 1 refers to the Index or &#8220;slot&#8221; &#8211; you can have up to five slots
*   NAME &#8211; IP Address names the variable &#8220;IP Address&#8221; in Analytics
*   VALUE &#8211; The php script echos the visitor&#8217;s ip address as the value for the above named variable
*   SCOPE &#8211; The final &#8220;1&#8243; defines the scope of the variable. Possible scopes include: 
    *   1 &#8211; visit level
    *   2 &#8211; session level
    *   3 &#8211; page level (default value)

There is a lag of about 12-24 hours before the IP addresses will show in analytics. To access this data click on Visitors->Custom Variables. A variable titles &#8220;IP&#8221; should appear in the list. Click on it to get detailed statistics. It should look something like this:

<div style="clear:both">
  &nbsp;
</div>

<center>
  <a href="http://ryanunderdown.com/uploads/2010/12/analytics-ip.png"><img src="http://ryanunderdown.com/uploads/2010/12/analytics-ip-300x122.png" alt="Google Analytics with IP Tracking" title="analytics-ip" width="300" height="122" class="aligncenter size-medium wp-image-1032" /></a>
</center>

<div style="clear:both">
  &nbsp;
</div>

Go [here][2] to read more about Custom Variables in Google Analytics.</a>

**UPDATE: Apparently this is against Analytic&#8217;s [TOS][1]. So if you are collecting this data it might be wise NOT to.**

The applicable section is: 

> &#8220;7. PRIVACY . You will not (and will not allow any third party to) use the Service to track or collect personally identifiable information of Internet users, nor will You (or will You allow any third party to) associate any data gathered from Your website(s) (or such third parties&#8217; website(s)) with any personally identifying information from any source as part of Your use (or such third parties&#8217; use) of the Service. You will have and abide by an appropriate privacy policy and will comply with all applicable laws relating to the collection of information from visitors to Your websites. You must post a privacy policy and that policy must provide notice of your use of a cookie that collects anonymous traffic data.&#8221;

 [1]: http://www.google.com/analytics/tos.html
 [2]: http://code.google.com/apis/analytics/docs/tracking/gaTrackingCustomVariables.html