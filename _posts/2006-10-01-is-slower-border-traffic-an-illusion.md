---
title: Is Slower Border Traffic An Illusion?
author: Ryan Underdown
layout: post
permalink: /digg/is-slower-border-traffic-an-illusion.php
categories:
  - digg
---
For years, the Border Patrol&#8217;s Tucson sector has been the busiest and deadliest in the nation. But in fiscal 2006, arrests declined about 10 percent compared with 2005. And for the first time since the agency started tracking deaths in 1999, the 162 reported fatalities in the sector did not break a record, dropping from 216 in 2005.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.azcentral.com/news/articles/1001border1001.html
 [2]: http://digg.com/politics/Is_Slower_Border_Traffic_An_Illusion