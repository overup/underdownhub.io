---
title: 'Interest rates dropping!  Homeowners let out collective sigh'
author: Ryan Underdown
excerpt: |
  |
    Everyone knows the stock market is in<a href="http://www.marketwatch.com/news/story/Story.aspx?guid=%7B76D0B6A7%2D9D68%2D4516%2DADE9%2D3D0232AC18A2%7D&siteid="> bull mode</a> but one of the pieces of news getting lost in the shuffle is that this isn't hurting the housing markets.  Over the last month I've watched the yield on the ten year note drop from 5.13 to 4.87.
layout: post
permalink: /real-estate/interest-rates-in-free-fall-home-sellers-let-out-collective-sigh.php
wjt_diPostTopic:
  - business_finacnce
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - Finance
  - Real Estate
---
Everyone knows the stock market is in[ bull mode][1] but one of the pieces of news getting lost in the shuffle is that this isn&#8217;t hurting the housing markets. Over the last month I&#8217;ve watched the yield on the ten year note drop from 5.13 to 4.87. Here was today&#8217;s activity:

<a rel='lightbox' href='http://ryanunderdown.com/wp-content/uploads/2006/08/bonds.gif' alt='bond yield curve'><img src="http://ryanunderdown.com/wp-content/uploads/2006/08/bonds.gif" alt="bond yield curve" /></a>

What does this mean for interest rates? A drop from roughly 6.375% as a par rate to 5.875%.  
<img id="image21" src="http://ryanunderdown.com/wp-content/uploads/2006/08/rates.gif" alt="rates.gif" />  
This number doesnt exactly correlate to an APR as a lender will assess some points and fees and of course their are always closing costs which will increase the APR of the loan, but this is good news for people looking to purchase a home or refinance out of their existing ARM&#8217;s.

 [1]: http://www.marketwatch.com/news/story/Story.aspx?guid=%7B76D0B6A7%2D9D68%2D4516%2DADE9%2D3D0232AC18A2%7D&#038;siteid=