---
title: U.S. to Deploy 4 Patriot Interceptors in Okinawa
author: Ryan Underdown
layout: post
permalink: /digg/us-to-deploy-4-patriot-interceptors-in-okinawa.php
categories:
  - digg
---
The United States on Monday revealed a plan to deploy four launch pads for a U.S.-led missile defense system in Japan&#8217;s southernmost prefecture of Okinawa later this week and moved part of equipment for the system from a port to the U.S. Kadena Air Base amid local protest.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/10/02/D8KGIUC01.html
 [2]: http://digg.com/world_news/U_S_to_Deploy_4_Patriot_Interceptors_in_Okinawa