---
title: 'Digg to Top Users: QUIT NOW?'
author: Ryan Underdown
layout: post
permalink: /digg/digg-to-top-users-quit-now.php
DiggUrl:
  - 'http://digg.com/submit?phase=2&url=http://ryanunderdown.com/2006/11/03/digg-to-top-users-quit-now/&title=Digg+to+Top+Users%3A+QUIT+NOW%3F&bodytext=&topic='
DiggData:
  - 'href=http://digg.com/submit?phase=2&url=http%3A%2F%2Fryanunderdown.com%2F2006%2F11%2F03%2Fdigg-to-top-users-quit-now%2F&title=Digg+to+Top+Users%3A+QUIT+NOW%3F&bodytext=&topic='
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
aktt_notify_twitter:
  - yes
categories:
  - digg
tags:
  - digg
  - disintermediation
---
Seems like the changes to the front page promotion algorithm have been drastic. After scrolling through the last 48 hours worth of stories promoted to the front page I counted a total of 2 stories submitted by top 25 users on Digg. In fact one of my own <a rel="nofollow" href="http://digg.com/politics/The_Allen_Staff_Subdueing_Heckler_in_Pictures">submissions currently has 93 diggs</a> and no front page.<div style=align:center;'>

<a rel='lightbox' href="http://ryanunderdown.com/wp-content/uploads/2006/11/screenshot-1.png"><img src="http://ryanunderdown.com/wp-content/uploads/2006/11/ss_allen_thumb.thumbnail.png" /></a></div> 
I realize I&#8217;m hardly a top user at #34 but after the last few weeks mass deletion of accounts and now these drastic changes it looks like the Digg staff is looking to get rid of its frequent posters. Kevin or Jay care to comment?