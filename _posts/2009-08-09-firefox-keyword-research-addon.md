---
title: Firefox Keyword Research Addon
author: Ryan Underdown
excerpt: "A simple firefox search engine addon that makes it faster to do keyword research.  This script passes your keyword research query to seobook's keyword research tool."
layout: post
permalink: /seo/firefox-keyword-research-addon.php
aktt_notify_twitter:
  - yes
syntaxhighlighter_encoded:
  - 1
aktt_tweeted:
  - 1
categories:
  - scripting
  - SEO
tags:
  - addon
  - Firefox
  - keyword research
  - keywords
  - search
  - seobook
  - tool
---
If you are anything like me you probably use seobook&#8217;s [keyword rankings tool][1] enough to have it on speed dial. Luckily the script used to process results accepts a GET request. As a result I have made a simple firefox search engine addon to make it easier to get the keyword research you are looking for.

NOTE: I am not in any way affiliated with seobook or Aaron Wall. (Aaron if you&#8217;d like me to take this down send me a note &#8211; Ryan)

xml for search engine:

<pre class="brush: xml; title: ; notranslate" title="">&lt;OpenSearchDescription&gt;
&lt;ShortName&gt;seobook keyword research&lt;/ShortName&gt;
&lt;Description&gt;seobook keyword research
&lt;/Description&gt;
&lt;Image height="16" width="16" type="image/x-icon"&gt;http://seobook.com/favicon.ico&lt;/Image&gt;
&lt;Url type="text/html" method="get" template="http://tools.seobook.com/keyword-tools/seobook/?Keyword={searchTerms}"/&gt;
&lt;/OpenSearchDescription&gt;</pre>

To install this script just click the down arrow on your search engine bar in firefox and select &#8220;seobook keyword research&#8221;. Big thanks to Aaron Wall for creating such a useful tool.

 [1]: http://tools.seobook.com/keyword-tools/seobook/