---
title: 'Microsoft: Secure Wireless Networks With WEP?'
author: Ryan Underdown
layout: post
permalink: /linux/microsoft-secure-wireless-networks-with-wep.php
DiggUrl:
  - http://www.digg.com/security/microsoft_secure_wireless_networks_with_wep
DiggData:
  - href=http://www.digg.com/security/microsoft_secure_wireless_networks_with_wep
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Linux
  - wifi
---
I was planning on spending some time today rebutting a recent article on securing your wireless network on [digg.com][1] so I thought why not check out the trusty reference guide over at [support.microsoft.com][2]. I started digging around and found [this gem][3]. By now anyone that has a wireless router and cares the slightest bit about preventing unwanted intrusion knows that bypassing WEP (Wireless Equivalency Protocol) is trivial.( as I pointed out in my recent tutorial on [cracking WEP with BackTrack][4]). Apparently the folks over at Microsoft didn&#8217;t get the memo.

Here are some highlights from [the article][3]:

> *   Enable the highest level of WEP that your hardware provides. WEP provides some security and is effective in deterring casual attempts by outsiders to infiltrate your network. Most 802.11b certified products can use basic 64-bit WEP encryption. By default, however, 64-bit WEP encryption may be disabled.

That alone ought to eat up about 15 seconds of a wifi hackers time.

> *   Change the default Service Set Identifier (SSID) and passwords for your network devices. Access points/wireless routers ship from the manufacturer with default SSID and passwords which is the same on all devices made by that manufacturer. Leaving these at default makes it easy for a malicious outsider to gain access.

That would actually be good advice if they were coupling it with WPA encryption. [Kismet][5] can sniff out an ssid even when it&#8217;s not &#8220;broadcast&#8221;. 

> *   Purchase access points and network adapters that support 128-bit WEP. Some products only support 64-bit (40 bit key) WEP, and are not as secure. Note that some adapters may only require a driver upgrade to attain 128-bit WEP capability.

Brilliant.

> *   Some access points allow you to control access based on the media access control address of the network adapter trying to associate with it. If the media access control address of your adapter is not in the table of the access point, you will not associate with it. If your access point has this feature, enable it and add the media access control addresses of the network adapters you use.

Your wireless adapter&#8217;s MAC address is broadcast constantly. Kismet or Kismac picks it up in a heartbeat. Spoofing your mac address might be [a little harder in windows][6], but it&#8217;s a snap on the mac or linux. You can use ifconfig or macchanger and many other simple scripts.

So how *should* you secure your home wireless network? Well the home user should obviously ditch WEP for WPA. WPA uses a much stronger encryption technique. It can be cracked with a brute force attack but if you name your SSID with a long obscure name and use a sufficient passkey it will be very difficult for a hacker to compromise your network. The reason this is more effective is that the encrypted key is computed by using the SSID (broadcast name) of the router AND the passkey itself. For this reason WPA is not very secure unless you randomize your SSID by [changing it from the default setting][7], ie linksys, default, belkin etc, AND use a random password generator (A good one is [available here][8]) you are fooling yourself.

 [1]: http://www.digg.com/security/How_to_Secure_A_Wireless_LAN
 [2]: http://support.microsoft.com
 [3]: http://support.microsoft.com/kb/309369
 [4]: http://ryanunderdown.com/2007/02/12/cracking-wep-using-backtrack/
 [5]: http://www.kismetwireless.net/
 [6]: http://www.irongeek.com/i.php?page=security/changemac
 [7]: http://www.wigle.net/gps/gps//Stat
 [8]: http://www.kurtm.net/wpa-pskgen/