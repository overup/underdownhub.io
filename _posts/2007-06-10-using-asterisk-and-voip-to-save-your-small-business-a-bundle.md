---
title: Using Asterisk And VOIP To Save Your Small Business A Bundle
author: Ryan Underdown
layout: post
permalink: /business/using-asterisk-and-voip-to-save-your-small-business-a-bundle.php
DiggUrl:
  - http://digg.com/linux_unix/using_asterisk_voip_to_save_your_small_business_a_bundle
DiggData:
  - href=http://digg.com/linux_unix/using_asterisk_voip_to_save_your_small_business_a_bundle
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
secondary_image:
  - http://ryanunderdown.com/images/asterisk.png
aktt_notify_twitter:
  - yes
categories:
  - Asterisk
  - Business
  - Linux
  - Trixbox
  - voip
tags:
  - Asterisk
  - pbx
  - sip
  - Trixbox
  - voip
---
**Overview**

By now most people have heard of the open source PBX <a rel="nofollow" href="http://asterisk.org">Asterisk</a> and what it can do, but how does this translate into savings for your small business? I have written this brief HOWTO to show how it has saved my company over $500 per month by leveraging the synergy of open source software and VOIP.

**Why use VOIP?**

The cost savings from using VOIP (Voice Over IP) over the traditional PSTN (public switched telephone network) are tremendous. By eliminating the need for using traditional phone companies expensive infrastructure (a switched phone line originating at your office and terminating at your customer&#8217;s), a small business can completely eliminate the need to pay per-minute long distance fees and outrageous access charges levied by Ma Bell to use their antiquated telephone network.

**Why use Asterisk?**

Anyone that has started up a new business office knows behind labor and their leased space, phones often rank third. For one you must buy or lease a PBX (personal branch exchange) from a company like Avaya. On top of the lease for equipment you pay for the actual lines and usage charges for local and long distance calls. If your business has a high call volume you could easily be paying a small fortune. Asterisk was created by <a rel="nofollow" href="http://www.marko.net/">Mark Spencer</a> the CTO of [Digium][1] and is licensed under the GPL which has attracted lots of third party add ons that increase its functionality. What does this mean for the small business owner? No software licensing fees, no hardware leasing and the power of a fully functional pbx with robust features built for voip.

**This sounds great! How do I start saving money with Asterisk?**

**THE GAMEPLAN**

There are a few steps that we must take in order to get started with our new high tech phone system. 

1.  Choose a VOIP provider
2.  Choose a broadband internet provider (DSL/T1)
3.  Purchase a computer to install Asterisk on
4.  Install Asterisk
5.  Purchase Phones
6.  Configure Asterisk to Communicate with Provider

**Choosing a VOIP Provider**

For my new office we chose [Inphonex][2]. They offer unlimited 1-800 calling plans which was very important to us as we have a high inbound call volume and have to offer an 800 number. Inphonex is Asterisk-Friendly, and was the only business class service that offered an unlimited 800 plan. They are based out of Miami and have written a HOWTO on how to setup asterisk servers to connect to their service as well. After three months I haven&#8217;t experienced any down time with their service.

There are a few other providers I considered such as Teliax,Telasip, viatalk. Perhaps someone could post in comments about their service or others?

**Choosing A Broadband Provider**

When choosing a broadband provider there are several considerations to take into account. The most important is the number of phones your company will require along with the compression codecs the provider supports. My new office requires only 4 phones at the moment. There are [several][3] [bandwidth][4] <a href="http://www.newport-networks.com/pages/voip-bandwidth-calculator.html" rel="nofollow">calculators</a> that can guide you through your selection of required ip bandwidth and codec selection. A quick check of the <a href="http://www.inphonex.com/knowledgebase/voice-call-quality-answers.php?action=view&#038;kb_id=28&#038;tag_id=" rel="nofollow"> Inphonex website</a> shows what codecs they supports. They offer support for several codecs including G.723, G.729a, G.726 ulaw and alaw. This provides you with some compression options in case you need several lines over a DSL line. 

**Building Your Server**

I built a bare bones computer by purchasing a case, motherboard, cpu, hard drive and network card at NewEgg. Any old computer will do, but starting on a fresh machine ensures to some degree that components will work for some time to come. Since this machine isn&#8217;t running any Microsoft bloatware, the machine doesn&#8217;t really need to be a supercomputer. My computer cost a total of $200. One of the advantages of running it on a linux server is that you can SSH into the machine remotely and manage it or you can access the web enabled GUI. I am using [trixbox][5] which comes with [FreePBX][6] already installed which makes managing your asterisk server easy even if you&#8217;ve never used linux or SSH before.

**Installing Asterisk**

I have installed asterisk on several different linux distros but the easiest way to get asterisk up and running on a dedicated server is to download trixbox. Trixbox is basically a linux distribution that grew out of the Asterisk At Home distribution of a few years back. Trixbox uses a database back end to store configuration values but other than that it is very similar to a typical Asterisk server compiled from the vanilla source. Trixbox also bundles in several features including an apache server, mysql database, FreePBX web based management, HUD, flash operator panel, and community based <a rel="nofollow" href="http://www.trixbox.org/forum">forums</a> that are very helpful in solving configuration issues.

You will need to:

*   Download an [ISO image][7]
*   Read the [quick start guide][8]
*   Read Inphonex&#8217;s <a rel="nofollow" href="http://www.inphonex.com/support/trixbox-configuration.php">configuration guide</a> for trixbox
*   Refer to the <a rel="nofollow" href="http://www.trixbox.org/forum">trixbox forums</a> for help

**Choosing Your Phones**

I settled on [GPX-2000][9] phones from <a rel="nofollow" href="http://www.grandstream.com/">Grandstream</a>. I bought them from 888voipstore and everything went very smoothly and I got a very reasonable price. I paid $78.95 per phone for phones with every feature I could want. Configuring them to work with my Asterisk server was extremely easy as well. Grandstream phones come with a web based GUI:

<center>
  <br /> <h3>
    Click to Enlarge
  </h3>
  
  <p>
    <a href='http://ryanunderdown.com/wp-content/uploads/2007/06/gs1.GIF' title='GrandStream Back End'><img src='http://ryanunderdown.com/wp-content/uploads/2007/06/gs1.GIF' height="300px" width="400px" alt='GrandStream Back End' /></a></center>
  </p>
  
  <p>
    Basically you only have to enter in the LAN/IP address of your asterisk box as your sip server and your phone works. Of course you still have to set up an extension on your asterisk box as well. Navigate in FreePBX to Settings-> Extensions and set up an extension number and choose a password like this:
  </p>
  
  <p>
    <center>
      <br /> <h3>
        Click to Enlarge
      </h3>
      
      <p>
        <a rel="nofollow" href='http://ryanunderdown.com/wp-content/uploads/2007/06/ext.GIF' title='extension setting'><img src='http://ryanunderdown.com/wp-content/uploads/2007/06/ext.GIF' height="300px" width="400px" alt='extension setting' /></a></center>
      </p>
      
      <p>
        <strong> Configure Asterisk for Inphonex</strong>
      </p>
      
      <p>
        Inphonex is an asterisk friendly provider. Conveniently they have a simple <a rel="nofollow" href="http://www.inphonex.com/support/trixbox-configuration.php">configuration guide</a> to setting up trixbox to work with their service. My working configuration is here:
      </p>
      
      <blockquote>
        <p>
          <code>allow=ulaw&lt;br />
canreinvite=yes&lt;br />
context=from-inphonex&lt;br />
disallow=all&lt;br />
fromdomain=sip.inphonex.com&lt;br />
fromuser=[myusernumber]&lt;br />
host=sip.inphonex.com&lt;br />
insecure=very&lt;br />
nat=yes&lt;br />
pedantic=no&lt;br />
qualify=yes&lt;br />
secret=[mypassword]&lt;br />
type=peer&lt;br />
username=[myusernumber]</code>
        </p>
      </blockquote>
      
      <p>
        My Register String:<br /> <center>
          <br /> <h3>
            Click to Enlarge
          </h3>
          
          <p>
            <a href='http://ryanunderdown.com/wp-content/uploads/2007/06/inphonepbx.GIF' title='Inphonex register string'><img src='http://ryanunderdown.com/wp-content/uploads/2007/06/inphonepbx.GIF' height='300px' width='400px' alt='Inphonex register string' /></a></center>
          </p>
          
          <p>
            For further help you can always search the <a rel="nofollow" href="http://www.trixbox.org/search/node/inphonex">trixbox forums</a> or contact <a href="http://www.inphonex.com/support/support.php">Inphonex&#8217;s support</a>.
          </p>

 [1]: http://www.digium.com/en/index.php
 [2]: http://inphonex.com/
 [3]: http://www.erlang.com/calculator/lipb/
 [4]: http://www.bandcalc.com/
 [5]: http://www.trixbox.org/
 [6]: http://www.freepbx.org/
 [7]: http://www.trixbox.org/downloads
 [8]: http://forge.trixbox.org/gf/project/trixbox2/wiki/?section=project&#038;ref_id=4&#038;pagename=trixbox+quick+install+guide
 [9]: http://www.888voipstore.com/grandstream-gxp-2000-pr-16165.html