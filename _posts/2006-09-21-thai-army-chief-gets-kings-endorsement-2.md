---
title: 'Thai Army Chief Gets King&#8217;s Endorsement'
author: Ryan Underdown
excerpt: "The army commander who seized Thailand's government in a quick, bloodless coup pledged Wednesday to hold elections by October 2007, and received a ringing endorsement from the country's revered king. Gen. Sondhi Boonyaratkalin also hinted that ousted Prime Minister Thaksin Shinawatra may face prosecution."
layout: post
permalink: /digg/thai-army-chief-gets-kings-endorsement-2.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
The army commander who seized Thailand&#8217;s government in a quick, bloodless coup pledged Wednesday to hold elections by October 2007, and received a ringing endorsement from the country&#8217;s revered king. Gen. Sondhi Boonyaratkalin also hinted that ousted Prime Minister Thaksin Shinawatra may face prosecution.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/20/D8K8KA400.html
 [2]: http://digg.com/world_news/Thai_Army_Chief_Gets_King_s_Endorsement