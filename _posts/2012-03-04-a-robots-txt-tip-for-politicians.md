---
title: A Robots.txt Tip for Politicians
author: Ryan Underdown
excerpt: "There's a simple solution to letting some things slip down the memory hole... namely blocking the internet archive bot.  If you are a politician you should IMMEDIATELY add the following to a robots.txt file on all of your server's document roots."
layout: post
permalink: /seo/a-robots-txt-tip-for-politicians.php
categories:
  - SEO
tags:
  - internet archive
  - politics
---
I&#8217;m amazed at how many times I&#8217;ve heard some variant of: &#8220;But Senator/Governor, your website said in 2006 that (you hated children|opposed access to birth control|approved of the individual mandate).&#8221; There&#8217;s a simple solution to letting some things slip down the memory hole&#8230; namely blocking the internet archive bot. If you are a politician you should IMMEDIATELY add the following to a robots.txt file on all of your server&#8217;s document roots:



ia_archiver is the name of the crawler for the internet archive. Luckily it is a friendly bot that obeys robots.txt directives. The other three bots listed are just remnants from my default robots.txt file. Dotbot blocks Seomoz&#8217;s opensiteexplorer, MJ12bot is MajesticSEO&#8217;s crawler and Ahrefsbot is for &#8211; you guessed it ahrefs.com. As these are primarily seo intelligence tools, blocking them makes sense if you are actively engaging in seo. Politicians mileage may vary.

In addition every page should carry the following header:  


Finally – if you find something you need removed (on your own website) on Google you can always follow [these directions][1] and on Bing follow [these directions][2].

 [1]: http://support.google.com/webmasters/bin/answer.py?hl=en&#038;answer=1663419
 [2]: http://www.bing.com/community/site_blogs/b/webmaster/archive/2009/01/29/removing-content-from-the-live-search-index.aspx