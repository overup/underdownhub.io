---
title: '10 Political Sites That Violate Digg&#8217;s TOS'
author: Ryan Underdown
layout: post
permalink: /politics/10-political-sites-that-violate-diggs-tos.php
DiggUrl:
  - http://digg.com/tech_news/10_sites_that_violate_digg_s_tos
DiggData:
  - href=http://digg.com/tech_news/10_sites_that_violate_digg_s_tos
aktt_notify_twitter:
  - yes
code_errorcode_worpress_seo:
  - 1
url_errorcode_worpress_seo:
  - http://indexedcontent.com/smo/10-sites-violate-digg-tos/
categories:
  - digg
  - Politics
  - SMO
tags:
  - astrotu
  - astroturfing
  - digg
  - libertarian diggers
  - ron paul
  - sockpuppets
  - spam
---
Digg.com&#8217;s inclusion of a politics section has given many political sites a springboard to attract a larger audience. For some of these groups the temptation to promote their political advocacy has proven too strong to stop them from violating Digg&#8217;s terms of service. The websites listed in this article appear to have crossed the line to varying degrees.

Article 3 Section 9 in [Digg&#8217;s Terms of Service][1]:

> By way of example, and not as a limitation, you agree not to use the Services:
> 
> 9. with the intention of artificially inflating or altering the &#8216;digg count&#8217;, blog count, comments, or any other Digg service, including by way of creating separate user accounts for the purpose of artificially altering Digg&#8217;s services; giving or receiving money or other remuneration in exchange for votes;** or participating in any other organized effort that in any way artificially alters the results of Digg&#8217;s services.**

1.  <a rel="nofollow" href="http://www.freecentury.com/2007/05/21/100000-diggs-for-ron-paul-show-your-support/">www.freecentury.com</a>
> In an effort to make sure that Ron Paul has a permanent place in Digg&#8217;s recent front page articles for a long time to come, we are launching the 100,000 Diggs for Ron Paul campaign.
> 
> The purpose is easily explained. We Ron Paul supporters want to get his message out to people who are not too familiar with him, and what he stands for. With the mainstream media ignoring the coming tsunami of Ron Paul support, many people might not hear about Dr. Paul until it is too late.
> 
> Digg this article and have everyone you know Digg it too.

2.  <a rel="nofollow" href="http://blog.mises.org/archives/005285.asp">Mises Institute</a>  
    > I previously explained about Digg.com and how their recent addition of non-technology topics (e.g. Politics, Business & Finance) has opened an opportunity for pro-market/pro-liberty articles to get an airing at this very popular news site.
    > 
    > Stories are &#8220;promoted&#8221; to the Digg.com front page by &#8220;digging&#8221; (voting) for a story. To coordinate efforts to promote free market and libertarian articles I have started a list of libertarian diggers. As I write this I already have 45 people on it. We have once again this morning been successful in promoting an article. In this case, today&#8217;s Mises.org Daily Article on inflation and the Fed. Head over to Digg and join the heated discussion about the article, (you will need to set up a free account).
    > 
    > If you want to join our merry band of libertarian diggers, here are the details&#8230;
    > 
    > E-mail me and send me your Digg Username (you login to Digg with this). I will add you to my list of Friends which is serving as our list of libertarian diggers.

3.  <a rel="nofollow" href="http://ronpaulreport.squarespace.com/the-ron-paul-report/2007/4/16/41607-ron-paul-daily-digg.html">The Ron Paul Report</a>  
    > 4/16/07 Ron Paul Daily Digg
    > 
    > Read and Digg <a href="http://ronpaulreport.squarespace.com/the-ron-paul-report/2007/4/16/41607-ron-paul-daily-digg.html" rel="nofollow">these articles</a>

4.  <a href="http://news.freetalklive.com/" rel="nofollow">http://www.freetalklive.com/</a>  
    > Digg FTL  
    > Digg.com is one of the most popular websites on the Internet. With your help, we can advance in their podcast rankings. Please click the button on the left to digg Free Talk Live. If you don&#8217;t have one, you will need to create a free account. Remember, you need to come back every two weeks and digg something else on the site like one of our episodes to continue locking in your digg for the podcast.  
    > digg.freetalklive.com -Direct link to the Free Talk Live page on digg.com news.freetalklive.com &#8211; Join the Free Market Diggers and help digg pro-Liberty articles on digg.com!

5.  <a href="http://groups.yahoo.com/group/RonPaulGrassRootsAction/message/837" rel="nofollow">Ron Paul Grass Roots Action</a>  
    > Hello,
    > 
    > This email message is a notification to let you know that  
    > a file has been uploaded to the Files area of the RonPaulGrassRootsAction  
    > group.
    > 
    > File : /Digg tips  
    > Uploaded by : tangent4ronpaul <tangent4ronpaul>  
    > Description : Using and networking on Digg for Ron Paul</tangent4ronpaul>
    > 
    > You can access this file at the <a href="http://groups.yahoo.com/group/RonPaulGrassRootsAction/files/Digg%20tips" rel="nofollow">URL</a>

6.  <a href="http://libertus93.blogspot.com/2007/05/ten-ways-to-help-online-ron-paul.html" rel="nofollow">No Governor Anywhere: FreeMarket, Libertarian Minded Blogging</a>  
    > Ten ways to help Ron Paul online.
    > 
    > We have to face it, the internet is currently the only place we can get the word out about Ron Paul.
    > 
    > 5. Digg.com: Go register at digg.com and do a search for Ron Paul. We have a double task here. First, read through the articles before you digg them. If you notice something posted twice, only digg the one that has the most so far, and bury the other. This way, people on digg won&#8217;t become burnt out by The Ron paul campaign. We can&#8217;t be called spammers if we only post one article about a certain topic, and if everyone is digging the same articles, the likelihood that story being on the front page, with the most exposure, increases.

7.  <a href="http://my.opera.com/Armitage/blog/2007/04/19/save-freedom-in-america-support-ron-paul" rel="nofollow">Armitage 2.0</a>  
    > &#8220;Digg Ron Paul sites, stories, videos, and other links.&#8221;

8.  <a href="http://www.worldaffairsboard.com/political-discussions/38800-digging-ron-paul-president-easiest-way-help-him.html" rel="nofollow">World Affairs Board</a>  
    > For those who want to help make sure Dr. Paul&#8217;s message gets out&#8230;
    > 
    > Here&#8217;s the most effective and easy way I know:
    > 
    > - Visit Digg / News
    > 
    > - Keyword search &#8220;Ron Paul&#8221;
    > 
    > - Then &#8220;digg&#8221; every story that has the word Ron Paul in the headline.
    > 
    > That&#8217;s it!
    > 
    > This gets him additional attention; I&#8217;ve seen it work very well before. It&#8217;s already working for Dr. Paul.
    > 
    > Usually there are around 25 Ron Paul stories on Digg every day. I Digg them about three times a day. What are your thoughts about this approach and about Ron Paul?

9.  <a href="http://www.ronpaulpresshub.com/?cat=13" rel="nofollow">Ron Paul Press Hub</a>  
    > Sunday, May 13th, 2007
    > 
    > Please digg the <a href="http://www.digg.com/politics/Tech_President_the_Web_on_the_Candidates" rel="nofollow">following article</a>. If you don&#8217;t know what Digg is, read here

10. <a href="http://disinter.wordpress.com/2007/03/12/chuck-hagel-vs-ron-paul/" rel="nofollow">disinter &#8220;digging up the truth&#8221;</a>  
    > For a brief comparison of Hagel vs Paul votes, go here. Digg the comparison <a rel="nofollow" href="http://www.digg.com/2008_us_elections/Sen_Chuck_Hagel_vs_Rep_Ron_Paul">here</a>.
    
    note: this is one of many articles intended to encourage diggers to promote Ron Paul</li> </ol> 
    **UPDATE**
    
    I found a few more:
    
    <a href="http://dailypaul.com/node/75" rel="nofollow">Daily Paul</a>
    
    > 10 Ways to help Ron Paul Every Day Without Leaving Your Computer  
    > Posted April 2nd, 2007 by manystrom  
    > in
    > 
    > * Ron Paul
    > 
    > 1. Go to digg.com and &#8216;digg&#8217;all of the stories about Ron Paul (also &#8216;digg&#8217; stories about him on other sites such as www.RonPaul2008.com and LewRockwell.com etc by simply clicking the &#8216;digg&#8217; button that is on most stories now). Making positive comment on those stories is a big help too. Also see reddit.com
    
    **<a href="http://www.bivingsreport.com/2007/hacking-digg-for-ron-paul/" rel="nofollow">BivingsReport</a>**
    
    Explains to readers how to resubmit a Ron Paul article by adding a # anchor to the end of the url.
    
    Other sites of interest:
    
    http://www.searchronpaul.com/

 [1]: http://digg.com/tos