---
title: Deep Questions Remain in Duke Rape Case
author: Ryan Underdown
layout: post
permalink: /digg/deep-questions-remain-in-duke-rape-case.php
categories:
  - digg
---
There&#8217;s not much middle ground in the legal community when it comes to opinions of local prosecutor Mike Nifong&#8217;s most famous case. His law school classmate Patricia McDonald, citing &#8220;an utter lack of evidence that a crime even occurred,&#8221; wrote to Gov. Mike Easley and urged him to pressure Nifong into stepping aside in the Duke University &#8230;

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/10/07/D8KJTDEG0.html
 [2]: http://digg.com/world_news/Deep_Questions_Remain_in_Duke_Rape_Case