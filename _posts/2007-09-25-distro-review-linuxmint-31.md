---
title: 'Distro Review: LinuxMint 3.1'
author: Ryan Underdown
layout: post
permalink: /linux/distro-review-linuxmint-31.php
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
secondary_image:
  - http://indexedcontent.com/images/mint.png
aktt_notify_twitter:
  - yes
categories:
  - Linux
  - OSS
tags:
  - desktop
  - Linux
  - linuxmint
---
*</p> 
### Part 1 in a 2 part series

</em>

I recently had the opportunity to try out <a rel="nofollow" href="http://www.linuxmint.com/">Linux Mint</a>, a new linux distribution aimed at capturing desktop user share from Microsoft. Linux Mint is a <a rel="nofollow" href="http://www.ubuntu.com/" rel="nofollow">Ubuntu</a> (<a rel="nofollow" href="http://www.us.debian.org/">debian</a>) based distribution with some interesting user interface improvements, a few notable additions, and some closed source files that Ubuntu doesn&#8217;t ship with. I know first hand that when my windows user friends come over and use my laptop to check their email, they are often taken aback by the graphical user interface differences between linux and windows. 

# **Where Linux Mint Excels:**

## **1. A User Friendly Interface**

I know first hand that when my windows user friends come over and use my laptop to check their email, they are often taken aback by the graphical user interface differences between linux and windows. In windows everyone knows that the start key is on the bottom left hand side of the screen. In a typical Gnome-based Ubuntu distribution of course it defaults to the top left and doesn&#8217;t even appear as an obvious button. Windows users don&#8217;t know what the hell they are looking at. Linux Mint gets around this by removing the top bar and putting the &#8216;start&#8217; (or program access) key at the bottom left of the screen. Where Ubuntu drops the ball, Linux Mint does a fairly good job of avoiding confusion for windows users. 

<a rel="nofollow shadowbox" href='http://ryanunderdown.com/wp-content/uploads/2007/09/desktop.png'  title='Linux Mint Desktop Screenshot'><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/desktop-small.png' alt='Linux Mint Desktop Screenshot' /></a>

As you can see from the screenshot the start button is not quite as obvious as a windows XP or Vista start key with the highly identifiable Microsoft logo, but a good deal more intuitive than a <a href="http://www.ubuntu.com/files/u3/desktop-tn.png" rel="nofollow">typical Gnome desktop</a>.

## **2. The Start Menu**

The start menu looks just like Windows XP/Vista. Menu items are grouped in a logical manner consistent with the average desktop user&#8217;s experience.

<a rel='shadowbox' href='http://ryanunderdown.com/wp-content/uploads/2007/09/startmenu.png' title='Linux Mint Start Menu'><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/startmenu-small.png' alt='Linux Mint Start Menu' /></a>

## **3. The Control Panel**

One of the most daunting things to a brand new-to-linux user is finding the locations of all the individual component-specific control panels. We have a font control a display control, a desktop control, an accessibility, a network, and countless other controls typically listed in the drop down menu under the system tab when Microsoft has them all neatly located in a &#8216;Control Panel&#8217; dialog box. Linux Mint organizes them in a nicely organized intuitive display as well:

<a rel='shadowbox' href='http://ryanunderdown.com/wp-content/uploads/2007/09/control-center.png' title='Linux Mint Control Panel'><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/control-center-small.png' alt='Linux Mint Control Panel' /></a>

## **4. Graphics Card Support**

Mint ships with a nice little installer called &#8216;Envy&#8217; for installing non-free video drivers from ATI and Nvidia. Unfortunately my graphics card on my laptop is an ATI radeon mobility which uses the standard driver so I was unable to test it out. In addition since my graphics card is so pathetically old I am unable to use the Open GL Beryl desktop. Mine defaulted to the standard gnome. However, it does come bundled with beryl and emerald. Here is a picture of some Emerald window decorations you can choose from:

<a rel='shadowbox' href='http://ryanunderdown.com/wp-content/uploads/2007/09/emerald-small.png' title='emerald control panel'><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/emerald-small.png' alt='emerald control panel' /></a>

## **5. Wifi Support**

Strangely my wireless adapter (which uses the fairly standard atheros chipset) didn&#8217;t work with the live cd version of Mint, but once I installed it to my drive it worked without a hitch. For unsupported wireless adapters, Mint comes with a handy NDIS wrapper installer so you can just insert your wireless card&#8217;s install disk, browse to it and install it for use in Mint. 

## **6. MintDisk: NTFS read/write and drive mounting out of the box**

Mint disk is a handy little tool that automatically mounts ntfs partitions &#8211; and not with the typical read only access that you get from a vanilla ubuntu install. Worked perfectly for me and could work hand in glove with ndiswrapper to install windows wireless ethernet drivers on dual boot machines. 

<a rel='shadowbox' href='http://ryanunderdown.com/wp-content/uploads/2007/09/mintdisk.png' title='mintdisk ntfs support'><img src='http://ryanunderdown.com/wp-content/uploads/2007/09/mintdisk-small.png' alt='mintdisk ntfs small' /></a>

I have only had a couple of hours to play with it so I expect to have a lot more to say in part 2.