---
title: 'Digg: Astroturfing For Obama'
author: Ryan Underdown
excerpt: "Company with ties to Obama campaign's David Axelrod caught astroturfing for the Democratic presidential nominee.  Looks like Axelrod's been gaming Digg as well."
layout: post
permalink: /politics/astroturfing-for-obama.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
secondary_image:
  - http://ryanunderdown.com/wp-content/uploads/2008/09/astroturf.png
lead_image:
  - http://ryanunderdown.com/wp-content/uploads/2008/09/astroturfl.png
categories:
  - Politics
tags:
  - astroturfing
  - digg
  - obama
---
Tonight I read Rusty Schackleford&#8217;s post in which he seemingly caught a [PR firm connected to the Obama campaign][1] astroturfing for Obama by posting and spreading inaccurate videos about Sarah Palin on Youtube. While this may not seem like anything new for people who have been using the major social media outlets, the question of who financed the videos that all strangely use the same voice over voice as recent Obama videos creates some campaign finance law issues.

Being a former digg &#8220;frequent submitter&#8221; I know of hundreds of now popular websites that employed this same technique to increase eyeballs and ultimately ad revenue with success. I have even [called out][2] Ron Paul supporters who took the technique to the next level. I had never heard of astroturfing, as I don&#8217;t run in political circles but according to Shackleford it is nothing Digg users should be unfamiliar with:

> David Axelrod is also known as The master of &#8220;Astroturfing&#8221;, which is what PR industry insiders call the practice of &#8220;manufacturing grassroots support.&#8221;

Now being thoroughly familiar with the manipulations of Digg by political entities I ran a simple search for sarah palin on digg and lo and behold I came across plenty of sock puppet accounts that hit Digg&#8217;s equivalent of hitting Powerball &#8211; 1 story submitted, 1 story promoted to the front page.

*   http://digg.com/users/BeerGodd
*   http://digg.com/users/linocut &#8211; joined Sep 10 2008
*   http://digg.com/users/stevenrl &#8211; joined Sep 9 08
*   http://digg.com/users/manucpa &#8211; joined Sep 7 2008
*   http://digg.com/users/carlisle99
*   http://digg.com/users/Bri345 &#8211; joined Aug 31st 2008 (From Wasilla Alaska no less!)

What do you want to bet these accounts only &#8216;digg&#8217; dirt on McCain/Palin?

You see astroturfing has been going on at Digg.com since day one. Most stories are promoted to the front page through a very active digg community subset that is constantly harassing me on IM to digg their stories. Everyone knows it has been going on at Digg, Republicans have just been too slow and stupid to catch on.

 [1]: http://mypetjawa.mu.nu/archives/194057.php
 [2]: http://indexedcontent.com/smo/10-sites-violate-digg-tos/