---
title: Why You Get So Much Chinese Blogspam
author: Ryan Underdown
excerpt: "If you're anything like me you have 10,000 spam comments in your spam moderation queue.  How much of yours points to .cn domains?"
layout: post
permalink: /seo/why-do-i-get-so-much-chinese-spam.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
secondary_image:
  - http://indexedcontent.com/images/baidu3.png
aktt_notify_twitter:
  - yes
categories:
  - SEO
tags:
  - baidu
  - china
  - spam
---
If you&#8217;re anything like me you have 10,000 spam comments in your spam moderation queue. Every once in a while I check whats going on there. Maybe its the fact that my girlfriend lives 2,000 miles away or maybe I should just own up to the truth that I have no life. Either way you can&#8217;t tell me you haven&#8217;t seen your share of &#8220;Buy WoW Gold&#8221; in blog comments somewhere. Most blog authoring systems like wordpress and blogger automatically add the nofollow tag to urls in comments by default, making them useless right? Wrong. 

![baidusucks1.jpg][1]

Baidu.com, which has 60%+ market share of all searches in China doesn&#8217;t recognize the nofollow attribute that Google invented years back to combat blogspam. Baidu treats every link the same &#8211; No relevancy issues, no PageRank, no reason not to run wild with automated link building.

 [1]: http://ryanunderdown.com/wp-content/uploads/2008/02/baidusucks1.jpg