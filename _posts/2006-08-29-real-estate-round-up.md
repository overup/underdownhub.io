---
title: Real Estate Round Up
author: Ryan Underdown
excerpt: Real estate news from August 29th 2006
layout: post
permalink: /real-estate/real-estate-round-up.php
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
aktt_notify_twitter:
  - yes
categories:
  - Finance
  - Real Estate
---
*   <a rel="nofollow" href="http://www.usatoday.com/money/industries/banking/2006-08-29-lender-losses-usat_x.htm?csp=34">Mortgage lenders see stock prices sink </a>
*   <a rel="nofollow" href="http://www.realestatejournal.com/buysell/taxesandinsurance/20060829-mcqueen.html?mod=RSS_Real_Estate_Journal&#038;rejrss=frontpage">Homeowners under insured due to surge in home prices</a>
*   <a rel="nofollow" href="http://www.realestatejournal.com/buysell/agentsandbrokers/20060829-hoak.html?mod=RSS_Real_Estate_Journal&#038;rejrss=frontpage">If you&#8217;re ready to list your home with an agent, you&#8217;ll have plenty of candidates. There are a record 2.6 million licensed real-estate agents in the U.S. &#8212; &#8220;way too many,&#8221; says Stefan Swanepoel, chief executive officer of Realty U, a real-estate education and training company in Aliso Viejo, Calif.</a>
*   <a rel="nofollow" href="http://www.redherring.com/Article.aspx?a=18228&#038;hed=Yahoo+Revamps+Real+Estate+Site&#038;sector=Industries&#038;subsector=InternetAndServices">The new, revised Yahoo Real Estate went live with new features including interactive maps, aerial views of homes, and a valuation comparison tool. The site is loaded with about 3 million real estate listings, provided by Prudential Real Estate.</a>
*   <a rel="nofollow" href="http://www.indystar.com/apps/pbcs.dll/article?AID=/20060828/BUSINESS/60828016/-1/ZONES01">Growing mortgage company will add 245 jobs</a>