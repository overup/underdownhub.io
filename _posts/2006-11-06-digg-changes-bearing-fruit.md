---
title: Digg Changes Bearing Fruit
author: Ryan Underdown
layout: post
permalink: /digg/digg-changes-bearing-fruit.php
DiggUrl:
  - 'http://digg.com/submit?phase=2&url=http://ryanunderdown.com/2006/11/06/digg-changes-bearing-fruit/&title=Digg+Changes+Bearing+Fruit&bodytext=&topic='
DiggData:
  - 'href=http://digg.com/submit?phase=2&url=http%3A%2F%2Fryanunderdown.com%2F2006%2F11%2F06%2Fdigg-changes-bearing-fruit%2F&title=Digg+Changes+Bearing+Fruit&bodytext=&topic='
categories:
  - digg
---
As I previously mentioned in comment on [zdnet][1], by taking away trusted top user&#8217;s motivation for posting disinterested content, political hacks, self interested bloggers and spammers will start taking over Digg&#8217;s front page. Here are three front page dupes in one day of that ilk. All three are at least slightly inaccurate. All three sponsored by members of a particular political ideation.

[  
Republicans sending out annoying robocalls pretending to be Democrats][2]

[  
GOP robo calling as Democratic candidates to annoy voters][3]  
[  
Republican fake phone call scandal spreads now in Philly too][4]

For good measure let&#8217;s throw in [Rhiannon1214][5]&#8216;s post as anyone that&#8217;s been paying attention knows that she&#8217;s part of the [Libertarian Diggers group][6] thats been [gaming Digg][7] for months.

[Want to Learn the US Constitution 5 Min Video Gives You Important Basics][8]

The end result is that Digg is starting to look just like [reddit.][9] If you don&#8217;t like the changes please head over to [Muhammad Saleem&#8217;s blog][10] and sign your name on to the open letter to Kevin Rose.

UPDATE:: a 4th has now appeared on the front page. [http://americablog.blogspot.com/2006/11/republicans-admit-they-are-breaking-to.html][11]

UPDATE 2:: a [5th stor][12]y has now gotten to the front page thanks to&#8230;. [a user that had never gotten a story to the front page before.][13] It seems the new algorithm is working out GREAT! /sarcasm  
Additional reading on the problems over at Digg can be found [here][14], [here][15], and [here][16]

 [1]: http://talkback.zdnet.com/5208-12517-0.html?forumID=1&#038;threadID=27039&#038;messageID=508737
 [2]: http://digg.com/politics/Republicans_sending_out_annoying_robocalls_pretending_to_be_Democrats
 [3]: http://digg.com/politics/GOP_robo_calling_as_Democratic_candidates_to_annoy_voters
 [4]: http://digg.com/politics/Republican_fake_phone_call_scandal_spreads_now_in_Philly_too
 [5]: http://digg.com/users/Rhiannon1214/dugg
 [6]: http://digg.com/politics/Return_of_the_Libertarian_Diggers
 [7]: http://bigdavediode.googlepages.com/diggfixexposed
 [8]: http://digg.com/political_opinion/Want_to_Learn_the_US_Constitution_5_Min_Video_Gives_You_Important_Basics
 [9]: http://reddit.com
 [10]: http://themulife.com/?p=244
 [11]: http://digg.com/politics/The_Republicans_admit_they_are_breaking_to_law_to_stop_people_from_voting
 [12]: http://digg.com/politics/Republicans_facing_hefty_fines_in_New_Hampshire
 [13]: http://digg.com/users/Tomanta/homepage
 [14]: http://www.deepjiveinterests.com/2006/11/04/kevin-rose-failing-social-media-101-miserably/
 [15]: http://plentyoffish.wordpress.com/2006/11/04/digg-fights-its-top-users/
 [16]: http://publishing2.com/2006/11/04/the-delicate-balance-of-participatory-media/