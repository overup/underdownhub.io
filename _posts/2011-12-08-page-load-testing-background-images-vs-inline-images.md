---
title: 'Page Load Testing &#8211; Background vs Inline Images'
author: Ryan Underdown
excerpt: 'I was recently working to speed up a website and was getting horrible load times for relatively small images (50k) off of a large amazon ec2 server.  I decided to create two simple pages and test if using background-images was making a noticeable difference.'
layout: post
permalink: /cro/page-load-testing-background-images-vs-inline-images.php
categories:
  - CRO
tags:
  - analytics
  - backgound-image
  - gwo
  - page load
---
I was recently working to speed up a website and was getting horrible load times for relatively small images (50k) off of a large amazon ec2 server. I decided to create two simple pages and test if using background-images was making a noticeable difference. My two test variants look like this:

### inline image test page



### Source for background image test page

  
You will notice that I kept the same number of elements (of the same type) on each of the pages. I left the source blank for the background image test variant to prevent polluting the test.

### results: inline image test page

[<img src="http://ryanunderdown.com/uploads/2011/12/test1-300x93.gif" alt="" title="_test1" width="300" height="93" class="aligncenter size-medium wp-image-1096" />][1]

### results: background image test page

[<img src="http://ryanunderdown.com/uploads/2011/12/test31-300x93.gif" alt="" title="_test3" width="300" height="93" class="aligncenter size-medium wp-image-1098" />][2]

The version *with* background images actually gave me a &#8220;DOM Ready&#8221; after .0225 seconds &#8211; while the fully loaded page load time was roughly the same as the inline image version. Could using all background images speed up firing of $.document(ready)? It turns out background-images are only downloaded after the element (containing div, span etc) is available. This prevents blocking from all the round trips required to get images. 

Full test results can be seen [here][3] and [here][4]. Additional reading can be found here:

*   [Does a browser download a background image with display:none?][5]
*   [Do Images Load Faster in HTML or CSS?][6]

 [1]: http://ryanunderdown.com/uploads/2011/12/test1.gif
 [2]: http://ryanunderdown.com/uploads/2011/12/test31.gif
 [3]: http://www.webpagetest.org/result/111208_2B_2EQYD/
 [4]: http://www.webpagetest.org/result/111208_EK_2ESPY/
 [5]: http://cloudfour.com/examples/mediaqueries/image-test/
 [6]: http://stackoverflow.com/questions/6716262/do-images-load-faster-in-html-or-css