---
title: Schwarzenegger Hacked?
author: Ryan Underdown
excerpt: |
  California Highway Patrol officials have opened a criminal investigation into "multiple" breaches and illegal downloads by outside hackers into the computers of Gov. Arnold Schwarzenegger's office, after an embarrassing private taped conversation was leaked last week to the Los Angeles Times, administration officials told The Chronicle.
layout: post
permalink: /politics/schwarzenegger-hacked-2.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - Politics
---
California Highway Patrol officials have opened a criminal investigation into &#8220;multiple&#8221; breaches and illegal downloads by outside hackers into the computers of Gov. Arnold Schwarzenegger&#8217;s office, after an embarrassing private taped conversation was leaked last week to the Los Angeles Times, administration officials told The Chronicle.

<a rel="nofollow" href="http://www.sfgate.com/cgi-bin/article.cgi?file=/c/a/2006/09/11/MNG8KL3A051.DTL">read more</a>&nbsp;|&nbsp;<a href="http://digg.com/politics/Schwarzenegger_Hacked" rel="nofollow">digg story</a>