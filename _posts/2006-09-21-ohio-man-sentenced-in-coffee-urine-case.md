---
title: Ohio Man Sentenced in Coffee Urine Case
author: Ryan Underdown
excerpt: "A former postal worker who poured urine into his co-workers' coffee must serve six months in a jail work-release program."
layout: post
permalink: /digg/ohio-man-sentenced-in-coffee-urine-case.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
A former postal worker who poured urine into his co-workers&#8217; coffee must serve six months in a jail work-release program. Thomas Shaheen, 50, of suburban Springfield Township, also must pay $1,200 to the people he used to work with to cover their cost of making a secret video of his role in tainting the office coffee.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/19/D8K85IFG0.html
 [2]: http://digg.com/world_news/Ohio_Man_Sentenced_in_Coffee_Urine_Case