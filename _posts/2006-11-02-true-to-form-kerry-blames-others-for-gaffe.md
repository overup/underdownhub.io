---
title: True to form, Kerry blames others for gaffe
author: Ryan Underdown
layout: post
permalink: /politics/true-to-form-kerry-blames-others-for-gaffe.php
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
categories:
  - Politics
---
If heâ€™s not putting his foot in his mouth, heâ€™s attempting to extract it, as he is again this morning, trying to blame everyone but himself for his outrageous defamation of Americaâ€™s fighting forces, slurring them as unproficient, shallow-minded cannon fodder.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://news.bostonherald.com/columnists/view.bg?articleid=165167
 [2]: http://digg.com/politics/True_to_form_Kerry_blames_others_for_gaffe