---
title: 'Today&#8217;s Good News in the Housing Market'
author: Ryan Underdown
excerpt: |
  |
    I know some people who read this blog must laugh at my unrequited optimism for the housing market, however here is today's support for my opinion that there will be another short term housing or at least a refinancing boom.
layout: post
permalink: /real-estate/todays-good-news-in-the-housing-market.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - Finance
  - Real Estate
---
I know some people who read this blog must laugh at my unrequited optimism for the housing market, however here is today&#8217;s support for my opinion that there will be another short term housing or at least a refinancing boom. 

From <a rel="nofollow" href="http://today.reuters.com/news/articlenews.aspx?type=reutersEdge&#038;storyID=2006-08-28T161544Z_01_N24474828_RTRUKOC_0_US-FINANCIAL-TREASURIES-CONVEXITY.xml">Reuters and Richard Leong</a>

> A further decline in benchmark Treasury yields may spur massive buying of government bonds by mortgage investors, adding zest to a market already spurred on by the Federal Reserve&#8217;s pause in interest-rate increases.
> 
> A tumble in Treasury yields on hopes the Fed may lower its key rate in 2007 has hurt returns on mortgage investments. As falling yields have lowered mortgage rates, homeowners have been enticed to refinance their loans.
> 
> As a result, some fund managers and loan-service companies have been forced into so-called convexity hedging, buying Treasuries or using interest-rate swaps to receive fixed-rate payments on their investments to make up for expected lost income from mortgage holdings.
> 
> &#8220;The mortgage market has been awoken here,&#8221; said Terry Belton, head of fixed income and derivatives strategy at J.P. Morgan Securities in Chicago. &#8220;The rally (in Treasuries) has triggered some significant buying by mortgage hedgers.&#8221;