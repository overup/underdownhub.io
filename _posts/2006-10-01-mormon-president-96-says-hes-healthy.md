---
title: 'Mormon President, 96, Says He&#8217;s Healthy'
author: Ryan Underdown
layout: post
permalink: /digg/mormon-president-96-says-hes-healthy.php
categories:
  - digg
---
Gordon B. Hinckley, president of the Church of Jesus Christ of Latter- day Saints, assured Mormons at their twice-yearly conference Sunday that his health is fine, despite major surgery this year. Hinckley, 96, was hospitalized for six days last winter after a cancerous growth was removed from his colon.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/10/01/D8KG3UQ00.html
 [2]: http://digg.com/world_news/Mormon_President_96_Says_He_s_Healthy