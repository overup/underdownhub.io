---
title: Fully automated DORM ROOM!
author: Ryan Underdown
layout: post
permalink: /digg/fully-automated-dorm-room.php
code_errorcode_worpress_seo:
  - 2
url_errorcode_worpress_seo:
  - http://ryanunderdown.com
categories:
  - digg
---
These guys (naturally at MIT) completely automated their dorm room. Lighting control, window blinds, security, everything! Videos and some technical info included. (found via Makezine: http://www.makezine.com/blog/archive/2006/05/  
midas\_homemade\_dorm\_room\_home.html)

[read more][1] | [digg story][2]

 [1]: http://web.mit.edu/zacka/www/midas.html
 [2]: http://digg.com/hardware/Fully_automated_DORM_ROOM_