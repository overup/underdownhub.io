---
title: 'Colo. School Attack &#8216;Sexual in Nature&#8217;'
author: Ryan Underdown
excerpt: |
  The gunman who took six girls hostage in a high school classroom, killing one, had sexually assaulted at least some of them, the sheriff said Thursday.  "He did traumatize and assault our children," Park County Sheriff Fred Wegener said. "I'll only say that it's sexual in nature."
layout: post
permalink: /digg/colo-school-attack-sexual-in-nature.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
The gunman who took six girls hostage in a high school classroom, killing one, had sexually assaulted at least some of them, the sheriff said Thursday. &#8220;He did traumatize and assault our children,&#8221; Park County Sheriff Fred Wegener said. &#8220;I&#8217;ll only say that it&#8217;s sexual in nature.&#8221;

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/28/D8KDUV5G0.html
 [2]: http://digg.com/world_news/Colo_School_Attack_Sexual_in_Nature