---
title: Why Democrats Are Losing The Culture War
author: Ryan Underdown
layout: post
permalink: /politics/why-democrats-are-losing-the-culture-war.php
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
categories:
  - Politics
---
Most voters worry about escalating challenges to family stability and the losing battle to instill good values in their children instead of the materialism and coarseness peddled by popular culture. They fear that our society has developed a casualness about life, especially as science has made it easier to manipulate and create beings.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://blogs.usatoday.com/oped/2006/10/post_44.html
 [2]: http://digg.com/political_opinion/Why_Democrats_Are_Losing_The_Culture_War