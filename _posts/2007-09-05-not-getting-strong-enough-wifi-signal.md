---
title: Not Getting Strong Enough Wifi Signal?
author: Ryan Underdown
excerpt: "Lifehacker told us we can easily boost our wifi antenna's reception up to 50% or more by creating a parabolic reflector out of scrap cardboard and tin foil.  I made a crude one following their directions and measured the improvement in signal reception on my laptop running kismet.  Check out my results!"
layout: post
permalink: /wifi/not-getting-strong-enough-wifi-signal.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
secondary_image:
  - http://indexedcontent.com/images/antenna.png
aktt_notify_twitter:
  - no
categories:
  - wifi
tags:
  - antenna
  - DIY
  - signal
  - wifi
---
Today&#8217;s post on [Lifehacker][1] showed how amazingly simple it is to make a parabolic reflector to increase your wifi device&#8217;s antenna range. Just download a template from [freeantennas.com][2] and grab some tinfoil out of the kitchen and you will be amazed with the results.

<img width='450px' src='http://ryanunderdown.com/wp-content/uploads/2007/09/antenna.jpg' alt='parabolic wifi antenna booster' />

I cut up an old manilla legal file folder, wrapped it up in tinfoil and made a makeshift holder by stapling cardboard across the middle for the antenna to pass through in about 5 minutes and it boosted my signal significantly. I&#8217;m sure with some tweaking I could get better results but this was all I needed for my local network to function properly. Here are some screenshots of the signal strength in kismet:

Without the antenna:

<img src='http://ryanunderdown.com/wp-content/uploads/2007/09/kismet-ant.png' width='450px' alt='kismet without the parabolic antenna showing lower db' />

With the antenna:

<img src='http://ryanunderdown.com/wp-content/uploads/2007/09/kismetant11.png' width='450px' alt='kismet showing signal strength with parabolic antenna' />

More or less a 50% improvement.

 [1]: http://lifehacker.com/software/how-to/boost-your-wireless-signal-with-a-homemade-wifi-extender-296367.php
 [2]: http://www.freeantennas.com/projects/template/index.html