---
title: Turning College Housing into an Investment Strategy
author: Ryan Underdown
excerpt: '<a href="http://www.forbes.com/2006/08/21/college-realestate-investments_cx_lr_0822aplus_5.html">Forbes</a> has released its list of the top college towns to invest in.  Phoenix - Mesa- Scottsdale came in at #7.  '
layout: post
permalink: /real-estate/turning-college-housing-into-an-investment-strategy.php
aktt_notify_twitter:
  - yes
categories:
  - Arizona
  - Real Estate
---
[Forbes][1] has released its list of the top college towns to invest in. Phoenix &#8211; Mesa- Scottsdale came in at #7. From the article:

> The good news is, college doesn&#8217;t need to be so hard on the wallet&#8211;at least when it comes to housing, an area that becomes more absurdly expensive with each passing year.
> 
> Total room-and-board expenses at private undergraduate colleges averaged $7,791 during the 2005-2006 school year, up 5% from the previous academic year, according to The College Board&#8217;s annual report on college pricing trends. But consider the alternative: investing in real estate. If done wisely, this nontraditional approach could not only save you the cost of college housing, it might even help you turn a profit.
> 
> Rather than shell out a small fortune for a ratty dorm room or an overpriced apartment, parents can build equity, generate cash flow and eventually benefit from real estate appreciation&#8211;assuming they are willing to be landlords and invest some cash up front. 
> 
> Rhonda Butler, sales manager at Fonville Morisey Realty&#8217;s Chapel Hill, N.C., office, near the University of North Carolina at Chapel Hill, breaks down a local example.
> 
> â€œSay you have a $200,000 townhouse,&#8221; she says. &#8220;You can take four students and charge them each $700 a month for rent, and suddenly you&#8217;re going to have a positive cash flow on the transaction.&#8221;
> 
> With 10% down, a 30-year mortgage and a rate of, say, 7%, monthly mortgage payments would total about $1,200, for a net of $1,600 per month.
> 
> And though home prices around the country appear to be flattening out after a few years of sharp increase, college housing markets tend to experience less ups and downs. 

I ran across a real world example [Here][2]

 [1]: http://www.forbes.com/2006/08/21/college-realestate-investments_cx_lr_0822aplus_5.html
 [2]: http://www.terramirabili.com/2006/08/arms_distance.htm