---
title: WordPress SEO Guide (ver 2.8.1)
author: Ryan Underdown
excerpt: 'I recently revisited this blog to make some SEO improvements. Wordpress does a great job of building internal links and with a few tweaks you can keep more link juice.  '
layout: post
permalink: /seo/wordpress-seo-guide.php
sandpaper:
  - 1
  - 1
sponge:
  - 1
aktt_notify_twitter:
  - yes
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_tweeted:
  - 1
categories:
  - SEO
  - Wordpress
tags:
  - guide
  - link building
  - noindex
  - pagerank
  - permalinks
  - robots.txt
  - SEO
  - Wordpress
---
I recently revisited this blog to make some SEO improvements. I&#8217;ve made a few structural changes to improve my site&#8217;s indexing that I&#8217;m happy to share. WordPress does a great job of building internal links and with a few tweaks you can squeeze those last precious drops of link juice out. While reading this please keep in mind that you probably shouldn&#8217;t make any of these changes to a live site, but if you do, disable any sitemap generation plugins you might have. If you ping Google while making iterative changes to your permalink structure you are bound to end up having Google send visitors to non-existent pages.

*   #### SEO-Friendly URLs
    
    Many WordPress blogs use date based permalinks (e.g. /2009/07/11/blog-title/), mainly because it&#8217;s the most seo-friendly option of the defaults available. However, using categories adds more relevant keywords to each url and removes a duplicate navigation entry point. As a result, I use the *category/post title* format. To set up these slightly more seo-friendly urls you must login to the admin screen, navigate to settings->permalinks and enter in the following custom values:
    
    <pre>/%category%/%postname%/</pre>

*   #### 301 Redirect Your Old URLs
    
    On an existing blog like this one, you don&#8217;t want to lose any back links pointing to your blog posts. In order to do this you will need to create 301 redirects from the old URLs to the new ones. A 301 redirect is a server code that gets sent to your browser informing it (transparently) that the content you were seeking is now *permanently moved* to a new location. The browser then automatically retrieves the new url without any input from the user. Googlebot also follows these server codes and will credit the new url specified with the back links pointed to the previous url. Setting up these redirects is typically done (on Apache servers) via a module called mod rewrite. Apache uses a special file named .htaccess to manage redirects. You can specify each redirect by hand in your .htaccess file (which could take forever on a large blog), programmatically via regex, or you can just download Dean Lee&#8217;s [Permalinks Migration Plugin][1] which does the trick without requiring you to learn mod rewrite syntax. Just install the plugin in your admin interface, specify both your old and new permalink structures and you are in the clear. It is always smart to make sure a 301 server code is being sent with the location of the new URL. If you use firefox, you can do this easily using the [live http headers addon][2]. When done successfully your server will send a code looking like this:
    
    <pre>HTTP/1.x 301 Moved Permanently
Date: Mon, 13 Jul 2009 17:22:49 GMT
Server: Apache/2.0.63 (Unix) mod_ssl/2.0.63 OpenSSL/0.9.8b mod_auth_passthrough/2.1 mod_bwlimited/1.4 FrontPage/5.0.2.2635
X-Powered-By: PHP/4.4.7
X-Pingback: http://ryanunderdown.com/xmlrpc.php
Expires: Wed, 11 Jan 1984 05:00:00 GMT
Cache-Control: no-cache, must-revalidate, max-age=0
Pragma: no-cache
Last-Modified: Mon, 13 Jul 2009 17:22:49 GMT
Location: http://ryanunderdown.com/digg/
Content-Length: 0
Keep-Alive: timeout=15, max=100
Connection: Keep-Alive
Content-Type: text/html; charset=UTF-8</pre>

*   #### Remove &#8220;Category&#8221; Slug from URLs
    
    I feel the &#8220;category&#8221; slug that WordPress adds to category page urls is one of the ugliest elements of the wordpress setup. It makes your urls inconsistent from an information retrieval standpoint. If your posts are structured &#8220;/category/post-title/&#8221; it makes sense for your category listings to be in the &#8220;/category/&#8221; folder. Unfortunately fixing it is a little difficult. After scouring the WordPress forums the best solution I&#8217;ve come up with<del datetime="2009-12-13T20:28:03+00:00"> goes against the &#8220;plugin&#8221; philosophy of WordPress &#8211; so use at your own risk</del> follows. You will need to download and install the [Top Level Categories Plugin][3]. This plugin takes care of removing the category slug on category pages. Next you must redirect your existing category page urls to the new slug-free ones by editing your .htaccess in your blog&#8217;s root directory. Mine looks something like this:
    
    <pre>RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
RedirectMatch 301 ^/category/(.+)$ http://ryanunderdown.com/$1
</pre>
    
    Note: if your category base uses a something other than category (ie: topics) you will need to adjust the above settings to match. Now the changes above work great except for on paginated categories (e.g. http://ryanunderdown.com/seo/page/2/). To [work around][4] this issue you need to update your permalink structure yet again. Go to &#8220;Settings->Permalinks&#8221; and set your custom URL structure to the following:
    
    <pre>/%category%/%postname%.php</pre>
    
    (while researching this post I was tempted to copy seoegghead&#8217;s use of the .seo extension for pages: http://www.seoegghead.com/software/wordpress-seo-pager.seo &#8211; interesting idea) </li> 
    *   #### Install Joost de Valk&#8217;s [Robots Meta Plugin][5]
        
        This is one of the more useful SEO plugins out there. It allows you to lock down the way your site gets spidered by adding noindex meta tags to dynamically generated pages like &#8220;/wp-login&#8221;. These are the settings I like:
        
        *   noindex comments rss
        *   noindex,follow search result pages
        *   noindex admin pages (login & register)
        *   subpages of the homepage
        *   disable author archives
        *   noindex your tags archives
        *   add noarchive, noodp and noydir tags
        *   disable author archives
        *   disable date based archives
        *   redirect search results when referrer is external
        *   #### Create A Sitemap
            
            Download the [Google Sitemap Generator][6] plugin. This tool gives you options as to what to specifically show Google. Make sure you uncheck &#8220;include archives&#8221;. We want Googlebot to spider our site through our categories. You want to avoid links to the archives in general if you can help it. </li> 
            *   #### Fix Your Titles
                
                WordPress likes to put your blog name first in post titles by default. To fix this mix up install the [All-In-One SEO Pack][7] plugin This little plugin switches up the order and gets rid of the annoying little &raquo; symbols wordpress inserts. In addition it gives you a few &#8220;per post&#8221; options &#8211; overall a decent tool.</li> 
                *   #### Deep Link Your Existing Content
                    
                    If you&#8217;ve ever read the online edition of the New York Times you certainly have noticed how they link back to their own articles about newsworthy items, events, and people. There&#8217;s good reason for this: links (and especially anchor text) are the currency of rankings. If you are linking to an article on your blog about [seo][8], make sure to use the anchor text &#8220;seo&#8221; not vague useless keywords like &#8220;click here&#8221;. I know of at least <span style="text-decoration: line-through">two</span> three plugins that will automatically link up keywords to existing content: [SEO Smart Links][9], [Batch Links][10] and (probably the best in the group) Gab Goldenberg&#8217;s [Internal Link Building][11]. These plugins basically require you to create a list of keyword and target url pairs. When a keyword is found in a subsequent blog post it is automagically linked to the appropriate target url (keyword landing page).</li> 
                    *   #### Disable Comment Links
                        
                        Just as inbound links increase your PageRank, outbound links drain it away &#8211; even if nofollowed. With the changes to the nofollow tag that have rendered it beyond useless, comment link droppers can put a hurt on your site&#8217;s link juice in a hurry. Hobo has a nice [plugin that disables comment links][12] for new posters.</li> </ul>

 [1]: http://www.deanlee.cn/wordpress/permalinks-migration-plugin/
 [2]: https://addons.mozilla.org/en-US/firefox/addon/3829
 [3]: http://fortes.com/projects/wordpress/top-level-cats/
 [4]: http://wordpress.org/support/topic/98005#post-550060
 [5]: http://yoast.com/wordpress/robots-meta/
 [6]: http://wordpress.org/extend/plugins/google-sitemap-generator/
 [7]: http://wordpress.org/extend/plugins/all-in-one-seo-pack/
 [8]: http://ryanunderdown.com/seo/
 [9]: http://www.prelovac.com/vladimir/wordpress-plugins/seo-smart-links
 [10]: http://kaloyan.info/blog/proekti/wordpress-proekti/wp-batch-links/
 [11]: http://seoroi.com/specialty-services/new-seo-plugin-for-wordpress-internal-link-building/
 [12]: http://www.hobo-web.co.uk/seo-blog/index.php/hobo-custom-link-love-for-wordpress/ "Hobo Custom Link Love"