---
title: Convicted Killer Freed on DNA Evidence
author: Ryan Underdown
excerpt: 'A man who was convicted in 1990 of raping and murdering a high school classmate when he was 16 was freed from prison Wednesday after DNA evidence implicated another man.  Jeffrey Deskovic, 33, hugged his attorney after a judge threw out the conviction, then was greeted by relatives after he was released.'
layout: post
permalink: /digg/convicted-killer-freed-on-dna-evidence.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
A man who was convicted in 1990 of raping and murdering a high school classmate when he was 16 was freed from prison Wednesday after DNA evidence implicated another man. Jeffrey Deskovic, 33, hugged his attorney after a judge threw out the conviction, then was greeted by relatives after he was released.

<a rel="nofollow" href="http://www.breitbart.com/news/2006/09/20/D8K8N5800.html">read more</a>&nbsp;|&nbsp;<a rel="nofollow" href="http://digg.com/world_news/Convicted_Killer_Freed_on_DNA_Evidence">digg story</a>