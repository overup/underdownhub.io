---
title: 'Mortgage Spam: You Might Be Surprised Who&#8217;s Behind It'
author: Ryan Underdown
excerpt: |
  |
    According to a recent articles from<a href="http://news.com.com/2100-1030_3-6094461.html">CNET</a>  and <a href="http://www.politechbot.com/2006/07/14/which-mortgage-lenders/">Politech</a>, one ISP has successfully fingered a handful of industry heavyweights:
layout: post
permalink: /finance/mortgage-spam-you-might-be-surprised-whos-behind-it.php
wjt_diPostTopic:
  - business_finance
aktt_notify_twitter:
  - yes
categories:
  - Business
  - Finance
---
According to a recent articles from[CNET][1] and [Politech][2], one ISP has successfully fingered a handful of industry heavyweights:

> White noticed that the spam flood had two things in common: It was being sent to many ASIS e-mail addresses that were no longer active, and it directed (she would later tell the judge) the recipient to connect to Web sites such as wwmort.com, bbmort.com and xxmort.com.
> 
> On Oct. 27, White filled out a form on one of the Web sites using the fictitious name of &#8220;Bruce Wolf.&#8221;
> 
> The next day, ASIS says, the company received this voice mail from Francis Prasad: &#8220;Hi, this message is for Bruce. Bruce, this is Francis calling from Aegis Lending Corporation (in) Sacramento. Bruce, actually, I am the loan officer who has been assigned to handle your financial request&#8230;&#8221; 
> 
> A legal brief that ASIS submitted includes transcripts of calls to &#8220;Bruce&#8221; from Aegis Lending, American Home Equity, Quicken Loans, Stateside Mortgage, Northstart Financial and National Fidelity Funding.

While I hear Quicken Loans has a wholesale side now for brokers I have never used them. AHE and Aegis however are two of the more notable names in the industry. Aegis is a generally known as a sub prime lender (although they do have a prime division now) and American Home Equity is a full service lender but their niche is stand alone seconds behind neg am loans. As the market continues to slide we can expect to see more of the same from companies desperate to increase sales in a declining market for shareholders.

 [1]: http://news.com.com/2100-1030_3-6094461.html
 [2]: http://www.politechbot.com/2006/07/14/which-mortgage-lenders/