---
title: Giambi Scolds A-Rod
author: Ryan Underdown
excerpt: |
  |
    Giambi is said to have told Torre: "Skip, it's time to stop coddling him."  The story says Rodriguez's teammates didn't like they way he refused to acknowledge his troubles despite looking more and more lost at the plate. From June 1 to Aug. 30, Rodriguez batted .257 with 14 homers and 52 RBIs, but faced constant criticism from fans and media
layout: post
permalink: /digg/giambi-scolds-a-rod-3.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
Giambi is said to have told Torre: &#8220;Skip, it&#8217;s time to stop coddling him.&#8221; The story says Rodriguez&#8217;s teammates didn&#8217;t like they way he refused to acknowledge his troubles despite looking more and more lost at the plate. From June 1 to Aug. 30, Rodriguez batted .257 with 14 homers and 52 RBIs, but faced constant criticism from fans and media &#8230;

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/na/cp_m091945A.xml.html
 [2]: http://digg.com/baseball/Giambi_Scolds_A_Rod