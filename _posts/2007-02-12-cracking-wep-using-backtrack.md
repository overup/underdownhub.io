---
title: 'Cracking WEP Using Backtrack: A Beginner&#8217;s Guide'
author: Ryan Underdown
layout: post
permalink: /linux/cracking-wep-using-backtrack.php
DiggUrl:
  - http://digg.com/security/cracking_wep_using_backtrack_a_beginner_s_guide
DiggData:
  - href=http://digg.com/security/cracking_wep_using_backtrack_a_beginner_s_guide
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
code_errorcode_worpress_seo:
  - 0
secondary_image:
  - http://ryanunderdown.com/images/backtrack.png
aktt_notify_twitter:
  - no
sandpaper:
  - 1
sponge:
  - 1
categories:
  - Linux
  - wifi
tags:
  - backtrack
  - encryption
  - network security
  - wep
  - wifi
  - wireless
---
<p><script>utmx_section("top-adsense")</script><br />
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3658299045266116";
/* top-wep */
google_ad_slot = "4420711088";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script><br />
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script><br />
</noscript>
<h2>A. SCOPE</h2>
<p>This tutorial is intended for user&#8217;s with little or no experience with linux or wifi.  The folks over at <a href="http://remote-exploit.org/">remote-exploit</a> have released &#8220;Backtrack&#8221; a tool which makes it ridiculously easy to access any network secured by WEP encryption.  This tutorial aims to guide you through the process of using it effectively.</p>
<h3>Required Tools</h3>
<ul>
<li>You will need a computer with a wireless adapter listed <a href="http://madwifi.org/wiki/Compatibility">here</a></li>
<li><a href="http://remote-exploit.org/backtrack_download.html">Download Backtrack</a> and burn it&#8217;s image to a CD</li>
</ul>
<h2>B. OVERVIEW</h2>
<p><strong>BACKTRACK</strong> is a bootable live cd with a myriad of wireless and tcp/ip networking tools.     This tutorial will only cover the included <a href="http://www.kismetwireless.net/">kismet</a> and <a href="http://www.aircrack-ng.org/doku.php">aircrack-ng</a> suite of tools.</p>
<h3>Tools Overview</h3>
<ul>
<li><strong>Kismet</strong> &#8211; a wireless network detector and packet sniffer</li>
<li><strong>airmon</strong> &#8211; a tool that can help you set your wireless adapter into monitor mode (rfmon)</li>
<li><strong>airodump</strong> &#8211; a tool for capturing packets from a wireless router (otherwise known as an AP)</li>
<li><strong>aireplay</strong> &#8211; a tool for forging ARP requests</li>
<li><strong>aircrack</strong> &#8211; a tool for decrypting WEP keys</li>
<li><strong>iwconfig</strong> &#8211; a tool for configuring wireless adapters. You can use this to ensure that your wireless adapter is in &#8220;monitor&#8221; mode which is essential to sending fake ARP requests to the target router</li>
<li><strong>macchanger</strong> &#8211; a tool that allows you to view and/or spoof (fake) your MAC address</li>
</ul>
<h3>Glossary of Terms</h3>
<ul>
<li><strong>AP</strong>: Access Point: a wireless router</li>
<li><strong>MAC Address</strong>: Media Access Control address, a unique id assigned to wireless adapters and routers.  It comes in hexadecimal format (ie 00:11:ef:22:a3:6a)</li>
<li><strong>BSSID</strong>: Access Point&#8217;s MAC address</li>
<li><strong>ESSID</strong>: Access Point&#8217;s Broadcast name. (ie linksys, default, belkin etc) Some AP&#8217;s will not broadcast their name but Kismet may be able to detect it anyway</li>
<li><strong>TERMINAL</strong>: MS-Dos like command line interface.  You can open this by clicking the black box icon next to the start key in backtrack</li>
<li><strong>WEP</strong>: short for Wired Equivalency Privacy, it is a security protocol for Wi-Fi networks</li>
<li><strong>WPA</strong>: short for WiFi Protected Access. a more secure protocal than WEP for wireless networks.  NOTE: this tutorial does not cover cracking WPA encryption</li>
</ul>
<p>Since Backtrack is a live CD running off your cdrom, there is nowhere that you can write files to unless you have a linux partition on your hard drive or a usb storage device.  Backtrack has some NTFS support so you will be able to browse to your windows based hard drive should you have one, but it will mount the partition as &#8220;read-only&#8221;.  I dual boot windows and ubuntu on my laptop so I already have a linux swap partition and a reiserfs partition.  Backtrack had no problem detecting these and mounting them for me. To find your hard drive or usb storage device, just browse to  the /mnt folder in the file manager.  Typically a hard drive will appear named something like hda1 or hda2 if you have more than one partition on the drive.  Alternately hdb1 could show if you have more than one hard disk.  Having somewhere to write files that you can access in case you need to reboot makes the whole process a little easier.  </p>
<h3>C. DISCLAIMER</h3>
<p>Hacking into someone&#8217;s wireless network without permission is probably against the law.  I wouldn&#8217;t recommend doing it.  I didn&#8217;t break into anyone else&#8217;s network while learning how to do this.</p>
<h3>D. IMPLEMENTATION</h3>
<h3>STEP 1</h3>
<h4>Monitoring Wireless Traffic With Kismet</h4>
<p>Place the backtrack CD into your cd-rom drive and boot into Backtrack.  You may need to change a setting in your bios to boot from cd rom.  During boot up you should see a message like &#8220;Hit ctrl+esc to change bios settings&#8221;.  Changing your first boot device to cdrom will do the trick.  Once booted into linux, login as root with username: root password: toor.  These are the default username and password used by backtrack.  A command prompt will appear.  Type startx to start KDE (a &#8216;windows&#8217; like workspace for linux).</p>
<p>Once KDE is up and running start kismet by clicking on the start key and browsing to Backtrack->Wireless Tools -> Analyzers ->Kismet.  Alternatively you can open a Terminal and type: </p>
<blockquote><p>
kismet
</p></blockquote>
<p>Kismet will start running and may prompt you for your wireless adapter. Choose the appropriate adapter, most likely &#8216;ath0&#8242;, and sit back as kismet starts detecting networks in range.</p>
<h3>NOTE: We use kismet for two reasons.</h3>
<ol>
<li> To find the bssid, essid, and channel number of the AP you are accessing.  </li>
<li> Kismet automatically puts your wireless adapter into monitor mode (rfmon).  It does this by creating a VAP (virtual access point?) or in other words, instead of only having ath0 as my wireless card it creates a virtual wifi0 and puts ath0 into monitor mode automatically.  To find out your device&#8217;s name just type:</li>
</ol>
<blockquote><p>iwconfig</p></blockquote>
<p>Which will look something like this:</p>
<p><a  href="http://ryanunderdown.com/uploads/2007/02/iwconfig.png" title='iwconfig.png'><img src="http://ryanunderdown.com/uploads/2007/02/iwconfig.thumbnail.png" alt='iwconfig.png' /></a></p>
<p>While kismet detects networks and various clients accessing those networks you might want to type &#8216;s&#8217; and then &#8216;Q&#8217; (case sensitive).  This sorts all of the AP&#8217;s in your area by their signal strength.  The default &#8216;autofit&#8217; mode that kismet starts up in doesn&#8217;t allow you much flexibility.  By sorting AP&#8217;s by signal strength you can scroll through the list with the arrow keys and hit enter on any AP you want more information on. (side note: when selecting target AP keep in mind this tutorial only covers accessing host AP&#8217;s that use WEP encryption.  In kismet the flags for encryption are Y/N/0.  Y=WEP N=Open Network- no encryption 0= other: WPA most likely.) Further reading on Kismet is available <a href="http://www.wi-fiplanet.com/tutorials/article.php/3595531">here</a>.</p>
<p>Select the AP (access point) you want to access.  Copy and paste the broadcast name(essid), mac address(bssid), and channel number of your target AP into a text editor. Backtrack is KDE based so you can use kwrite.  Just open a terminal and type in &#8216;kwrite&#8217; or select it from the start button. In Backtrack&#8217;s terminal to copy and paste you use shift+ctrl+c and shift+control+v respectively.  <strong>Leave kismet running to leave your wireless adapter in monitor mode.</strong>  You can also use airmon to do this manually. <code>airmon-ng -h</code> for more help with this</p>
<h2>STEP 2</h2>
<h3>Collecting Data With Airodump</h3>
<p>Open up a new terminal and start airodump so we can collect ARP replies from the  target AP.  Airodump is fairly straight forward for help with this program you can always type &#8220;airodump-ng -h&#8221; at the command prompt for additional options.</p>
<blockquote><p>
airodump-ng ath0 -w /mnt/hda2/home/ryan/belkin_slax_rcu 9 1
</p></blockquote>
<p>Breaking down this command:</p>
<ul>
<li> <strong>ath0</strong> is my wireless card</li>
<li><strong>-w</strong> tells airodump to write the file to<br /> /mnt/hda2/ryan/belkin_slax_rcu</li>
<li><strong>9</strong> is the channel 9 of my target AP</li>
<li><strong>1</strong> tells airodump to only collect IVS &#8211; the data packets with the WEP key</li>
</ul>
<p><script>utmx_section("adsense-code-middle")</script><br />
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3658299045266116";
/* mid-wep-sq */
google_ad_slot = "2784913175";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script><br />
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script><br />
</noscript>
<h2>STEP 3</h2>
<h3>Associate your wireless card with the AP you are accessing.</h3>
<blockquote><p>
aireplay-ng -1 0 -e belkin -a 00:11:22:33:44:55 -h 00:fe:22:33:f4:e5 ath0
</p></blockquote>
<ul>
<li><strong>-1</strong> at the beginning specifies the type of attack.  In this case we want fake authentication with AP.  You can view all options by typing <code>aireplay-ng -h</code>
	</li>
<li><strong>0</strong> specifies the delay between attacks</li>
<li><strong>-e</strong> is the essid tag.  belkin is the essid or broadcast name of my target AP.  Linksys or default are other common names</li>
<li><strong>-a</strong> is the bssid tag(MAC address).  00:11:22:33:44:55 is the MAC address of the target AP</li>
<li> <strong>-h</strong> is your wireless adapters MAC addy.  You can use macchanger to view and change your mac address. <code>macchanger -s ath0</code></li>
<li><strong>ath0</strong> at the end is my wireless adapters device name in linux</li>
</ul>
<h2>STEP 4</h2>
<h3>
Start packet injection with aireplay<br />
</h3>
<blockquote><p>
aireplay-ng -3 -b 00:11:22:33:44:55 -h 00:fe:22:33:f4:e5 ath0
</p></blockquote>
<h4>NOTES:  </h4>
<ul>
<li><strong>-b</strong> requires the MAC address of the AP we are accessing.</li>
<li><strong> -h</strong> is your wireless adapters MAC addy.  You can use macchanger to view and change your mac address. <code>macchanger -s ath0</code></li>
<li>if packets are being collected at a slow pace you can type<code>iwconfig ath0 rate auto</code> to adjust your wireless adapter&#8217;s transmission rate.  You can find your AP&#8217;s transmission rate in kismet by using the arrow keys up or down to select the AP and hitting enter.  A dialog box will pop up with additional information.  Common rates are 11M or 54M.
</li>
</ul>
<p>As aireplay runs, ARP packets count will slowly increase.  This may take a while if there aren&#8217;t many ARP requests from other computers on the network.  As it runs however, the ARP count should start to increase more quickly.  If ARP count stops increasing, just open up a new terminal and re-associate with the ap via step 3.  There is no need to close the open aireplay terminal window before doing this.  Just do it simultaneously.  You will probably need somewhere between 200-500k IV data packets for aircrack to break the WEP key.</p>
<h3>If you get a message like this:</h3>
<p><code>Notice: got a deauth/disassoc packet. Is the source MAC associated ?</code></p>
<p>Just reassociate with the AP following the instructions on step 3.</p>
<h2>STEP 5</h2>
<h3>Decrypting the WEP Key with Aircrack</h3>
<p>Find the location of the captured IVS file you specified in step 2.  Then type in a terminal: </p>
<blockquote><p>
aircrack-ng -s /mnt/hda2/home/belkin_slax_rcu-03.ivs
</p></blockquote>
<p>Change /mnt/hda2/home/belkin_slax_rcu-03.ivs to your file&#8217;s location</p>
<p>Once you have enough captured data packets decrypting the key will only take a couple of seconds.  For my AP it took me 380k data packets.  If aircrack doesn&#8217;t find a key almost immediately, just sit back and wait for more data packets.</p>
<p><a href='http://ryanunderdown.com/wp-content/uploads/2007/02/aircrack.png' title='aircrack.png'><img src='http://ryanunderdown.com/wp-content/uploads/2007/02/aircrack.thumbnail.png' alt='aircrack.png' /></a>
</p>
<p>If this guide doesn&#8217;t fully answer your questions you can always refer to the forums at <a href="http://forums.remote-exploit.org/archive/index.php/f-8.html">remote-exploit.org</a></p>
