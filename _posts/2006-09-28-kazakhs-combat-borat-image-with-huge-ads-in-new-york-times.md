---
title: 'Kazakhs Combat &#8220;Borat&#8221; Image with Huge Ads in New York Times'
author: Ryan Underdown
excerpt: |
  It might not be coming out until November, but the upcoming film "Borat," starring Sacha Baron Cohen as a faux-Kazakh buffoon, is making Kazakhstan deeply nervous about its national image -- so much so that the country's government took out a pricey four-page full-color ad in the New York Times and the International Herald Tribune today.
layout: post
permalink: /digg/kazakhs-combat-borat-image-with-huge-ads-in-new-york-times.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
It might not be coming out until November, but the upcoming film &#8220;Borat,&#8221; starring Sacha Baron Cohen as a faux-Kazakh buffoon, is making Kazakhstan deeply nervous about its national image &#8212; so much so that the country&#8217;s government took out a pricey four-page full-color ad in the New York Times and the International Herald Tribune today.

<a rel="nofollow" href="http://www.tmz.com/2006/09/27/kazakhs-buy-glorious-ad-to-combat-borat/">read more</a>&nbsp;|&nbsp;<a rel="nofollow" href="http://digg.com/movies/Kazakhs_Combat_Borat_Image_with_Huge_Ads_in_New_York_Times">digg story</a>