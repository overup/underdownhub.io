---
title: 'CEO Wealth Meter &#8211; Today&#8217;s Biggest Gainers and Losers'
author: Ryan Underdown
excerpt: Larry Ellison of Oracle gained $2.15 Billion while Eric Schmidt of Google lost $75.56 Million.
layout: post
permalink: /digg/ceo-wealth-meter-todays-biggest-gainers-and-losers.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
Larry Ellison of Oracle gained $2.15 Billion while Eric Schmidt of Google lost $75.56 Million.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://finance.breitbart.com/breitbart?Page=CEO
 [2]: http://digg.com/business_finance/CEO_Wealth_Meter_Today_s_Biggest_Gainers_and_Losers