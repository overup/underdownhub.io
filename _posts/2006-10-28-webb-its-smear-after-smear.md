---
title: 'Webb: It&#8217;s Smear After Smear'
author: Ryan Underdown
layout: post
permalink: /politics/webb-its-smear-after-smear.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Politics
---
Webb told Washington Post radio that to pull excerpts from his writings &#8220;and force them on people, sort of, like pound them over the head with them,&#8221; rather than having someone read the entire book &#8220;is just a classic example of the way this [Allen] campaign has worked. And you know, it&#8217;s smear after smear.&#8221;

<a rel="nofollow" href="http://www.cnn.com/2006/POLITICS/10/27/webb.allen/">read more</a>&nbsp;|&nbsp;<a rel="nofollow" href="http://digg.com/politics/Webb_It_s_Smear_After_Smear">digg story</a>