---
title: Blake Passes Off Ryan Adams Song As Tim McGraw
author: Ryan Underdown
layout: post
permalink: /music/blake-passes-off-ryan-adams-song-as-tim-mcgraw.php
DiggUrl:
  - http://digg.com/television/blake_passes_off_ryan_adams_song_as_tim_mcgraw
DiggData:
  - href=http://digg.com/television/blake_passes_off_ryan_adams_song_as_tim_mcgraw
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Music
---
I just got done watching American Idol (yes I&#8217;m sad) and my girlfriend and I recognized Blake&#8217;s song *When The Stars Go Blue* <a rel="nofollow" href="http://www.allmusic.com/cg/amg.dll?p=amg&#038;token=&#038;sql=33:jnfwxb85ldte">as a Ryan Adams song</a> when he said it was Tim McGraw. <a rel="nofollow" href="http://www.allmusic.com/cg/amg.dll?p=amg&#038;sql=10:kvfpxqrdldje">Tim McGraw actually covered Ryan Adams on a single</a>. Blake has consistently been praised as contemporary and cool when really he is just singing obscure songs.