---
title: 'Page Load Testing: Embedding With DocStoc and Scribd'
author: Ryan Underdown
excerpt: A quick comparison of loading time considerations when choosing between scribd and docstoc document embeds
layout: post
permalink: /seo/page-load-testing-embed-with-docstoc-and-scribd

categories:
  - SEO
tags:
  - docstoc
  - quantcast
  - scribd
---
<p>As I work in search engine marketing &#8211; I frequently test my sites&#8217; page load times just to make sure I&#8217;m getting my content to visitors as quickly as possible. I don&#8217;t spend a lot of time worrying about my blog &#8211; until I posted an embedded pdf on a post recently and noticed a significant performance drop off. I ran this site through the great tool at [Webpagetest.org][1]. I was appalled to see 10 second+ load times!!!1 I dug into the numbers and saw that my scribd embed was hogging bandwidth *like a sonofabitch*. I knew there are basically two players in this field &#8211; [scribd][2] and [docstoc][3] so i figured I would test page load time with each. The results are pretty interesting:</p>

<table cellpadding="0" cellspacing="0">
  <th>Page</th>
  <th>Browser</th>
  <th>Load Time</th>
  <th>Fully Loaded</th>
  <th>Req</th>
  <th>Bytes in</th>
  <tr>
    <td>DocStoc</td>
    <td><a rel="nofollow" href="http://www.webpagetest.org/result/121216_S1_15B/">Chrome</a></td>
    <td>&#10003; 0.616s</td>    
    <td>4.324s</td>    
    <td>12</td>
    <td>258 KB</td>
  </tr>  
  <tr>
    <td>DocStoc</td>    
    <td><a rel="nofollow" href="http://www.webpagetest.org/result/121216_YF_23C/">Firefox</a></td>
    <td>1.295s</td>    
    <td>3.681s</td>
    <td>12</td>    
    <td>258 KB</td>
  </tr>  
  <tr>
    <td>DocStoc</td>
    <td><a rel="nofollow" href="http://www.webpagetest.org/result/121216_AJ_14G/">IE8</a></td>
    <td>1.348s</td>
    <td>3.900s</td>
    <td>12</td>
    <td>258 KB</td>
  </tr>
  <tr>
    <td>Scribd</td>    
    <td><a href="http://www.webpagetest.org/result/121216_X0_23S/">Firefox</a></td>    
    <td>7.800s</td>    
    <td>8.556s</td>    
    <td>49</td>    
    <td>557 KB</td>
  </tr>
  
  <tr>
    <td>Scribd</td>  
    <td><a href="http://www.webpagetest.org/result/121216_EQ_158/">Chrome</a></td>    
    <td>8.378s</td>    
    <td>9.198s</td>    
    <td>57</td>    
    <td>552 KB</td>
  </tr>
  <tr>
    <td>Scribd</td>    
    <td><a rel="nofollow" href="http://www.webpagetest.org/result/121216_54_145/1/details/">IE8</a></td>
    <td>15.269s</td>    
    <td>15.755s</td>    
    <td>63</td>    
    <td>742 KB</td>
  </tr>
</table>

<p>DocStoc loads a flash object &#8211; while scribd loads up a remote page via an iframe. Intuitively one might expect flash to perform worse &#8211; but in this case the biggest culprit is the sheer volume of third party scripts scribd is embedding. Depending on browser between 49-63 request compared to DocStoc&#8217;s consistent 12. Additionally scribd serves up over twice the total page weight as does DocStoc (257kb vs 552-743kb). Scribd clearly isn&#8217;t concerned enuogh about user experience with their embedded documents.</p>

<p>Some notes about the test &#8211; first off its clearly unscientific &#8211; but the difference in load time was such that further tests seem pretty pointless. I used the default embed code from each site &#8211; and uploaded the same simple text document to each (a simple robots.txt file). Each page was identical and loaded no other external resources. The pages can be seen [here][4] and [here][5]. </p>

<p>A few items of interest &#8211; scribd is including twitter, facebook and google+ external javascripts which is probably not as noticeable on many sites &#8211; but still a very heavy bandwidth decision. The one item that stands apart though is scribd&#8217;s decision to include a quantcast tracking code (http://edge.quantserve.com/quant.js). Looks like a bid to inflate their quantcast numbers &#8211; [judging by their traffic there][6].</p>

 [1]: http://www.webpagetest.org
 [2]: http://www.scribd.com
 [3]: http://www.docstoc.com
 [4]: http://www.alliedmetro.com/docstoc-test.php
 [5]: http://www.alliedmetro.com/scribd-test.php
 [6]: http://www.quantcast.com/scribd.com
