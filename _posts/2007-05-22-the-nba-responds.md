---
title: The NBA Responds!
author: Ryan Underdown
layout: post
permalink: /arizona/the-nba-responds.php
categories:
  - Arizona
---
Thank you for taking the time to contact us about the suspensions of Amare Stoudemire and Boris Diaw of the Phoenix Suns. Although we probably will not change your mind, we wanted to share with you the rationale for the rule and the facts requiring our decision.

The Rule

Rule 12, Section VII(c) of the NBA Official Playing Rules says: &#8220;During an altercation, all players not participating in the game must remain in the immediate vicinity of their bench. Violators will be suspended, without pay, for a minimum of one game and fined up to $50,000.&#8221;

Â·  
The purpose of the rule is to prevent an on-court altercation from getting worse by making sure that players on the bench do not become involved &#8212; whether or not they intend to. The fewer the number of players on the court, the less likely it is that an altercation will escalate and the more likely it is that the referees and coaches will be able to restore order without serious injury to players or to fans.

Â·  
The rule doesn&#8217;t look to the intent of the players leaving the bench and it does not distinguish among the curious, the peacemakers or those seeking to become involved in the altercation. The reason for this is simple &#8212; the players on the court have no idea what a player&#8217;s intent is when he leaves the bench and in the heat of the moment they may well assume the player is approaching as an aggressor. Thus, the language of the rule is firm: &#8220;violators will be suspended.&#8221;

Â·  
This is not a rule that can be enforced on a case-by-case basis &#8212; if a player were able to leave the bench and later argue his case and avoid a suspension, there would be more players leaving the bench. And because the rule has been applied consistently over the years, bench-clearing incidents have been rare. Overall, the leaving-the-bench rule, together with others, has succeeded in dramatically reducing the amount of fighting in the league and all but eliminated serious injury during fights that do occur.

Â·  
Teams and players are reminded of the rule before every regular season and again before the playoffs. Teams try to ensure that their players comply with the rule by both reminding them of it and assigning assistant coaches the job of keeping players in the vicinity of the bench when incidents do occur.

The Facts Requiring the Stoudemire and Diaw Suspensions

Â·  
As soon as Steve Nash was fouled, both Amare and Boris ran toward the scene, each ending up over 20 feet away from the Suns&#8217; bench and near the altercation. Despite what many have said, they didn&#8217;t &#8220;walk&#8221; a few feet from the bench and they didn&#8217;t &#8220;wander&#8221; onto the court. In fact, they engaged in the very conduct the rule was meant to stop.

Â·  
No one knew what the players&#8217; intentions were when they left the bench and they could very easily have gotten involved in the altercation had it spilled over in their direction. And although you could say they were having a &#8220;natural&#8221; reaction to seeing their teammate go down, assistant coaches are supposed to stop bench players from acting on those kinds of reactions, which was the case with the 17 other players (active and inactive) who were on the benches at the time and did not leave.

As with all NBA rules, this one can be changed by a vote of the Board of Governors. Rules are typically changed following a recommendation from the Competition Committee, which will discuss the leaving-the-bench rule (along with several other items) at its next meeting. At this time, we donâ€™t have a better rule to recommend.

One thing everyone can agree on: this was an extremely unfortunate circumstance. We never want to suspend players for any game, much less a playoff game, but for all the reasons described above, the facts dictated the application of the rule.