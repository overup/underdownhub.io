---
title: 'Housing Boom 2.0 &#8211; How It Could Happen'
author: Ryan Underdown
excerpt: |
  |
    There are several signs that affordability could increase rapidly in the coming months.
layout: post
permalink: /real-estate/housing-boom-20-how-it-could-happen.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - Business
  - Finance
  - Real Estate
---
There are several signs that affordability could increase rapidly in the coming months.

*   Falling Interest rates &#8211; According to the <a rel="nofollow" href="http://www.realtor.org">National Association of Realtors</a> a 1% drop in interest rates increases buying power by 10%. In the last 3 months <a rel="nofollow" href="http://www.freddiemac.com/dlink/html/PMMS/display/PMMSOutputYr.jsp?year=2006">interest rates have dropped .32%</a> &#8211; theoretically a 3.2% increase in buying power. 
*   <a rel="nofollow" href="http://www.jacksonholestartrib.com/articles/2006/08/24/news/business/483382773f69c571872571d200776c53.txt">Inflation appears to be contained.</a> <a rel="nofollow" href="http://quote.bloomberg.com/apps/news?pid=20601087&#038;sid=apFKD6LwSg10&#038;refer=home">Treasury bulls are betting big on it.</a> Consumer prices appear to be in decline <a rel="nofollow" href="http://www.bloomberg.com/apps/news?pid=20601103&#038;sid=a9Vnl5qAs2Ak&#038;refer=us">globally</a> as well. 
*   <a rel="nofollow" href="http://www.statesman.com/business/content/business/stories/other/08/16reca.html">Some markets</a> are still appreciating rapidly. 
*   <a rel="nofollow" href="http://www.forbes.com/business/2006/08/23/home-prices-sales-cx_jh_0823homes.html">Even in the face of rising interest rates home have been holding their values</a>
*   Some analysts are predicting <a rel="nofollow" href="http://www.bloomberg.com/apps/news?pid=20601103&#038;sid=a9Vnl5qAs2Ak&#038;refer=us">an additional 28 bps drop</a> in the 10 Year&#8217;s yield which would increase the downward pressure on interest rates significantly. 
*   <a rel="nofollow" href="http://www.sirchartsalot.com/comments.php?chart_id=243">The US central bank has pursued a policy of &#8220;asset targeting&#8221; since 1999, adjusting short term interest rates in response to movements of stock market indexes and housing prices</a>. This is further evidence that the Fed could start lowering rates soon as more news of a slowdown in the stock market and housing continue.