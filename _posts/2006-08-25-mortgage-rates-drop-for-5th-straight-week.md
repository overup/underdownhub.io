---
title: Mortgage Rates Drop for 5th Straight Week
author: Ryan Underdown
excerpt: |
  |
    <a href="http://www.washingtonpost.com/wp-dyn/content/article/2006/08/25/AR2006082500551.html">It's official.</a>  Interest rates are on their way down.  <a href="http://www.freddiemac.com/dlink/html/PMMS/display/PMMSOutputYr.jsp?year=2006">Freddie Mac's report</a> is a lagging indicator as most customers don't know that they can expect a lower rate than they are being offered by their lender or broker because they have been hearing about rising rates for the last year and a half.
layout: post
permalink: /finance/mortgage-rates-drop-for-5th-straight-week.php
aktt_notify_twitter:
  - yes
categories:
  - Finance
---
[It&#8217;s official.][1] Interest rates are on their way down. [Freddie Mac&#8217;s report][2] is a lagging indicator as most customers don&#8217;t know that they can expect a lower rate than they are being offered by their lender or broker because they have been hearing about rising rates for the last year and a half. Most people in the industry are looking at interest rates with cautious optimism as there are [mixed messages from the Fed][3]. However Bernanke seems to have hit his stride as he didn&#8217;t rattle markets after his speech in jackson Hole today.

From [Briefing.com][4]

> Treasury Traders Day Off: The market wandered higher today, shifting up as some traders heaved a collective sigh of relief after (still new) Fed chief Bernanke refrained from letting any major market moving tape-bombs fly in his Jackson Hole speech. 

As a result the Yield on the 10 year bond shed 2 basis points. Keep your fingers crossed.

 [1]: http://www.washingtonpost.com/wp-dyn/content/article/2006/08/25/AR2006082500551.html
 [2]: http://www.freddiemac.com/dlink/html/PMMS/display/PMMSOutputYr.jsp?year=2006
 [3]: http://www.bloomberg.com/apps/news?pid=20601087&#038;sid=a2iGQNIXV.ao&#038;refer=home
 [4]: http://briefing.com/Investor/Public/MarketAnalysis/BondMarketUpdate.htm