---
title: 'Minview: Minimalist Mobile Browsing'
author: Ryan Underdown
layout: post
permalink: /scripting/minview-minimalist-mobile-browsing

categories:
  - scripting
tags:
  - browsing
  - minimalist
  - minview
  - mobile
---
<img src="http://ryanunderdown.com/wp-content/uploads/2011/02/20110201-094812.jpg" alt=""  />
  
Today I&#8217;m announcing a rapid prototype I&#8217;ve been working on for the past few days called [Minview][1]. Mobile browsing has come a long way since the dark days of the blackberry browser, but despite the recent advancements made by the iphone, palm and android devices &#8211; mobile browsers are still left wonting. While android and iphone&#8217;s webkit browsers let you zoom in on content formatted for desktop browsers, the presentation is frequently less than optimal. I was recently impressed by a fellow hacker&#8217;s implementation of a mobile version of [hacker news][2] that formats the desktop version into a highly usable mobile version with some added tricks. I encourage you to visit the site and check it out.

Unfortunately hacker news is just one of many sites I frequent on a day to day basis. So I created my own version to work on the sites I love. Minview currently has two basic ways of working. The first pulls an RSS feed of my favorite blogs and formats links to content for minimalist mobile viewing. As you can tell by visiting the default page for minview that I&#8217;m a conservative (atheist) so for the moment you may or may not be interested in the default implementation. The second, [mindrudge][3], parses [drudge report][4] and pulls all of the non static links &#8211; pushes them into an array and then passes those links off to my server-side implementation of &#8220;[readability][5]&#8220;.

Minview has a few cool features on top of the traditional readability script. Namely it:

*   parses images and embedded objects and automatically resizes them to fit in your mobile browser
*   updates the viewport when an iphone changes orientation (not sure if this works on android)
*   selects all links in parsed pages and prepends a link so they are automatically parsed via minview
*   can parse any page by sending the url to: minview.com/out/?url=[url here]

As always I appreciate your feedback and if you have requests for any sites to be added please let me know in the comments.

 [1]: http://minview.com
 [2]: http://ihackernews.com/
 [3]: http://minview.com/drudge/
 [4]: http://www.drudgereport.com
 [5]: http://lab.arc90.com/experiments/readability/
