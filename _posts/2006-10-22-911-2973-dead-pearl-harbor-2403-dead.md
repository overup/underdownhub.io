---
title: '9/11: 2,973 Dead &#8211; Pearl Harbor: 2,403 Dead'
author: Ryan Underdown
layout: post
permalink: /politics/911-2973-dead-pearl-harbor-2403-dead.php
DiggUrl:
  - 'http://digg.com/submit?phase=2&url=http://ryanunderdown.com/2006/10/22/911-2973-dead-pearl-harbor-2403-dead/&title=9%2F11%3A+2%2C973+Dead+-+Pearl+Harbor%3A+2%2C403+Dead&bodytext=&topic='
DiggData:
  - 'href=http://digg.com/submit?phase=2&url=http%3A%2F%2Fryanunderdown.com%2F2006%2F10%2F22%2F911-2973-dead-pearl-harbor-2403-dead%2F&title=9%2F11%3A+2%2C973+Dead+-+Pearl+Harbor%3A+2%2C403+Dead&bodytext=&topic='
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Politics
---
There has been endless talk over the last few years about the loss of constitutional rights after terrorists attacked the World Trade Center buildings on 9/11/01. Controversies have swirled around the adoption of the <a rel="nofollow" href="http://www.slate.com/id/2087984/">Patriot Act</a>, <a rel="nofollow" href="http://www.google.com/url?sa=t&#038;ct=res&#038;cd=19&#038;url=http%3A%2F%2Fwww.nytimes.com%2F2005%2F12%2F16%2Fpolitics%2F16program.html%3Fei%3D5090%26en%3De32072d786623ac1%26ex%3D1292389200&#038;ei=GoQ7RbmNC5G8gAPpg4CCBQ&#038;sig=___Oe4ozLrEc8gBaqcYLXtzdvVm9Q=&#038;sig2=iJNyMq2LTps3jUoItusqsA">warrantless wiretaps</a> on overseas calls to suspected terrorists and even the *<a rel="nofollow" href="http://www.nytimes.com/2006/06/23/washington/23intel.html?ei=5088&#038;en=168d69d26685c26c&#038;ex=1308715200&#038;partner=rssnyt&#038;emc=rss&#038;pagewanted=all">Swift</a>* program of tracking terrorists funds in overseas banks. However, some perspective might show that this concern might be a little exaggerated and in fact largely unfounded. 

On September 11, 2001 <arel ="nofollow" href="http://en.wikipedia.org/wiki/September\_11%2C\_2001_attacks#Fatalities">2,973 unsuspecting civilians were killed by 19 militant muslim hijackers adhering to a radical form of Islam working under the moniker of Al-Qaeda. On December 7, 1942 <a rel="nofollow" href="http://en.wikipedia.org/wiki/Pearl_harbor#Pearl_Harbor_after_December_7.2C_1941">2,403</a> military personnel were killed by the surprise attack put forth by the Japanese. The response by the American government to these attacks differed greatly. On February 12, 1942 Democratic president Franklin D. Roosevelt signed <a rel="nofollow" href="http://en.wikipedia.org/wiki/Japanese_interment#Executive_Order_9066_and_related_actions">Executive Order 9066</a> which was the legal basis for setting up Japanese interment camps for all persons of Japanese decent living on the west coast of the United States. Over <a rel="nofollow" href="http://en.wikipedia.org/wiki/Japanese_interment#_note-howmany">112,000</a> Japanese-Americans were rounded up and forced to live in interment camps until 1944. 

After the attacks on the WTC buildings no such camps were established for Muslim-Americans. In the interest of national security, the Bush administration authorized the NSA to secretly intercept calls originating in the United States that were calling suspected terrorists abroad without obtaining a warrant. The [rhetoric][1] <a rel="nofollow" href="http://www.worldcantwait.net/index.php?option=com_content&#038;task=view&#038;id=3036&#038;Itemid=220">from</a> [the][2] [left][3] has been [soaring][4] but the fact remains that this is hardly an unprecedented suspension of constitutional rights and in the opinion of this author a reasonable, proportionate response. </arel>

 [1]: http://ipsnews.net/news.asp?idnews=34650
 [2]: http://www.princeton.edu/~starr/articles/articles06/Starr-BushConstitution-3-06.htm
 [3]: http://www.counterpunch.org/lindorff07112006.html
 [4]: http://www.dailykos.com/story/2006/1/11/212726/954