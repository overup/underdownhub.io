---
title: 10 Videos You Should Bookmark
author: Ryan Underdown
layout: post
permalink: /music/10-great-music-videos-on-youtube.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Music
---
1.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Young Folks" href="http://www.youtube.com/v/51V1VMkuyx0&autoplay=1">Peter Bjorn &#038; John &#8211; Young Folks</a> 
2.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="I'd Rather Dance With You" href="http://www.youtube.com/v/C9r9sQ6PHOM&autoplay=1">Kings of Convenience &#8211; I&#8217;d Rather Dance With You</a> 
3.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Peach Plum Pear" href="http://www.youtube.com/v/KcHjAUhtSrk&autoplay=1">Joanna Newsom &#8211; Peach, Plum, Pear</a> 
4.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Cat Power - The Greatest" href="http://www.youtube.com/v/SDsxkQk6DWw&autoplay=1">Cat Power &#8211; The Greatest</a> 
5.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="DJ Shadow - High Noon" href="http://www.youtube.com/v/cLD7Krt3TFA&autoplay=1">DJ Shadow &#8211; High Noon</a> 
6.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="UNKLE (feat Thom Yorke) - Rabbit in your Headlights" href="http://www.youtube.com/v/5Q3uSnFXI2A&autoplay=1">UNKLE (feat Thom Yorke) &#8211; Rabbit in your Headlights</a> 
7.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Spoon - I Turn My Camera On" href="http://www.youtube.com/v/ro95Ns58qSE&autoplay=1">Spoon &#8211; I Turn My Camera On</a> 
8.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Feist - One Evening" href="http://www.youtube.com/v/rU3vGQn1Jjo&autoplay=1">Feist &#8211; One Evening<br /> (ok this video blows but I love Feist)</a> 
9.  <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Radiohead- Paranoid Android" href="http://www.youtube.com/v/r8asQsfn82E&autoplay=1">Radiohead- Paranoid Android (an oldie but a goodie)</a> 
10. <a rel="shadowbox[Movies];width=405;height=340;options={continuous:true}" class="option" title="Coconut Records - West Coast" href="http://www.youtube.com/v/mTzEp4CeWT8&autoplay=1">Coconut Records &#8211; West Coast</a>