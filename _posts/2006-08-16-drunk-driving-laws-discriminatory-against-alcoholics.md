---
title: 'Drunk Driving Laws: Discriminatory Against Alcoholics?'
author: Ryan Underdown
excerpt: 'In a <a href="http://www.hfes.org/Web/PubPages/celldrunk.pdf">recent study performed by David Strayer of the University of Utah</a> drivers talking on their cellphones performed significantly worse than drivers with a blood alcohol concentration (BAC) of 0.08%, <a href="http://www.ohsinc.com/drunk_driving_laws_blood_breath%20_alcohol_limits_CHART.htm">the legal limit in 47 out of 50 states</a>'
layout: post
permalink: /politics/drunk-driving-laws-discriminatory-against-alcoholics.php
aktt_notify_twitter:
  - yes
categories:
  - Politics
---
In a [recent study performed by David Strayer of the University of Utah][1] drivers talking on their cellphones performed significantly** worse **than drivers with a blood alcohol concentration (BAC) of 0.08%, [the legal limit in 47 out of 50 states][2]

> ![Drunk Drivers Vs Cell Phone Drivers][3]

This study begs the question, if the purpose of traffic laws is to protect the citizenry using public roads, are current laws against drunk drivers discriminatory? A driver caught talking on their cellphone is typically required to pay a fine and continue on their way, whereas a person who has had as little as one drink can have their license revoked and even their car seized depending on the state in which the ofense takes place. Could a drunk driver successfully overturn a conviction because the laws are discriminatory against a person with a disability? IANAL, comments?

 [1]: http://www.hfes.org/Web/PubPages/celldrunk.pdf
 [2]: http://www.ohsinc.com/drunk_driving_laws_blood_breath%20_alcohol_limits_CHART.htm
 [3]: http://ryanunderdown.com/wp-content/uploads/2006/08/drunkdrivers1.gif