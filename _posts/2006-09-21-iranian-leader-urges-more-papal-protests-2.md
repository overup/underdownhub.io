---
title: Iranian Leader Urges More Papal Protests
author: Ryan Underdown
excerpt: In Iran, supreme leader Ayatollah Ali Khamenei used the comments to call for protests against the United States. He argued that while the pope may have been deceived into making his remarks, the words give the West an "excuse for suppressing Muslims" by depicting them as terrorists.
layout: post
permalink: /digg/iranian-leader-urges-more-papal-protests-2.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
In Iran, supreme leader Ayatollah Ali Khamenei used the comments to call for protests against the United States. He argued that while the pope may have been deceived into making his remarks, the words give the West an &#8220;excuse for suppressing Muslims&#8221; by depicting them as terrorists.

<a rel="nofollow" href="http://breitbart.com/news/2006/09/18/D8K7H9OO0.html">read more</a>&nbsp;|&nbsp;<a rel="nofollow" href="http://digg.com/world_news/Iranian_Leader_Urges_More_Papal_Protests">digg story</a>