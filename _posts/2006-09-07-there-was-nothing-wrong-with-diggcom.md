---
title: There was nothing wrong with Digg.com
author: Ryan Underdown
excerpt: "I woke up this morning to the shit storm brewing at digg.com like the rest of you.  I however might have a somewhat unique perspective that I'd like to share.  "
layout: post
permalink: /digg/there-was-nothing-wrong-with-diggcom.php
DiggUrl:
  - http://digg.com/tech_news/There_was_nothing_wrong_with_Digg_com
aktt_notify_twitter:
  - yes
categories:
  - digg
  - SMO
tags:
  - controversy
  - digg
  - jay adelson
  - kevin rose
  - userbase
---
I woke up this morning to the shit storm brewing at digg.com like the rest of you. I however might have a somewhat unique perspective that I&#8217;d like to share. My digg.com account name is [elebrio][1]. To date I have had 16 stories promoted to the front page. You can view them here:

<http://digg.com/users/elebrio/homepage>

If you pay enough attention you will notice that although my account was created in November of 2005, I didn&#8217;t really start submitting stories until exactly 16 days ago. Thats right, as a neophyte I have gotten an average of 1 story promoted to the front page per day for the last 16 days. My account is proof that it isn&#8217;t impossible to break through the (non-existent) monopoly of the top users. How did I do it?

1. Activity. If you comment on and submit stories actively you will get more people to check out both your profile and submissions. Increasing your chances that others will digg your stories. I comment on many stories. Being the first commenter doesn&#8217;t hurt either as you automatically get top billing.

2. Write interesting blurbs. If you only have 3 or 4 sentences to get people to digg your stories, then you better not be boring or people will just scan over it. This is where p9 gets kudos from me. He writes up the best synopsises(?) hands down. Thats why he was the top user.

3. Digg &#038; Comment on your friends stories. If you digg your friends stories and comment on them, they are more likely to check out the stories that you submit.

That&#8217;s it! I&#8217;m seriously disappointed that Kevin and Jay decided to change things up instead of just let the free market run it&#8217;s course. Obviously there are some groups that needed to be weeded out &#8211; like the [libertarian diggers][2]. They were clearly gaming the system, but the fact that their decision lost them [p9s50W5k4GUD2c6][3] shows how easily they will piss away their userbase. Their arrogance when dealing with the [netscape][4] controversy doesn&#8217;t make me want to submit stories to their site so they can get rich and talk down to me like they did p9s50W5k4GUD2c6.

 [1]: http://digg.com/users/elebrio/profile
 [2]: http://digg.com/politics/Return_of_the_Libertarian_Diggers
 [3]: http://digg.com/users/p9s50W5k4GUD2c6
 [4]: http://netscape.com