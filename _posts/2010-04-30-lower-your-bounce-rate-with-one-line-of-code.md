---
title: Lower Your Bounce Rate With One Line of Code
author: Ryan Underdown
layout: post
permalink: /seo/lower-your-bounce-rate-with-one-line-of-code.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
categories:
  - Analytics
  - SEO
tags:
  - bounce rate
  - event tracking
  - javascript
  - SEO
---
While reading through the [Google Analytics Event Tracking Guide][1], I came across this nugget:

> In general, a &#8220;bounce&#8221; is described as a single-page visit to your site. In Analytics, a bounce is calculated specifically as a session that triggers only a single GIF request, such as when a user comes to a single page on your website and then exits without causing any other request to the Analytics server for that session. However, if you implement Event Tracking for your site, you might notice a change in bounce rate metrics for those pages where Event Tracking is present. This is because Event Tracking, like page tracking is classified as an interaction request. 

Of course having read through several SEO related posts identifying bounce rate as a [ranking factor][2] (or at a minimum a quality signal), I devised a way to game it. 

<pre><code class="javascript">&lt;div id="header" onMouseOver="pageTracker._trackEvent('bounce', 'bouncecheck', 'Look Ma No Bounce');"&gt;
</code></pre>

I figure a mouseover on my header will probably be triggered enough to dramatically drop my bounce rate without looking too artificially manipulated. Plus I wonder if a body onLoad statement would be overkill, or trigger faster than the gif request. Anyway after implementing this for one day you can see the huge difference below.  
<br style="clear:both" />  
<center>
  <img style="float:none" src="http://ryanunderdown.com/uploads/2010/04/bouncerate.jpg" alt="" title="bouncerate" width="188px" height="54px" />
</center>

  
<br style="clear:both" />

Now while my method is obviously solely aimed at gaming the system, there are some legitimate uses. A few examples that come to mind where firing off an event make sense are video plays, if the end of a javascripted animation, newsletter signups, rss subscribes and there are probably a thousand more.

 [1]: http://code.google.com/apis/analytics/docs/tracking/eventTrackerGuide.html
 [2]: http://seoblackhat.com/2008/11/21/bounce-rate-seo/