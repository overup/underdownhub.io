---
title: Digg Takes A Step Back From Disintermediation
author: Ryan Underdown
layout: post
permalink: /digg/digg-takes-a-step-back-from-disintermediation.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
secondary_image:
  - http://indexedcontent.com/images/digg.png
aktt_notify_twitter:
  - yes
categories:
  - digg
tags:
  - digg
  - disintermediation
---
One of the driving forces that led me to become a frequent user of Digg was the idea of [disintermediation][1] of news; that the userbase set the rules and determined what sites were newsworthy and which sites should be banned. In recent months following the DVD encryption key mayhem that rocked the site, things appear to have changed without much of anyone taking notice. 

Muhammad Saleem [noted][2] a while back:

> As I recall, according to Digg policy:
> 
> When submitted stories are consistently reported as spam and users complain via our feedback email about submission spam, we ban the domain. The domain will not be unbanned.

That section of the Terms of Use has recently been changed to:

> Digg may remove any Content and Digg accounts at any time for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content), or for no reason at all. To report Terms of Use abuse, please email: abuse@digg.com

A <a rel="nofollow" href="http://www.digg.com/search?s=prisonplanet.com&area=promoted&type=url&search-buried=1&sort=new&section=news">search</a> of front page promoted stories submitted from <a rel="nofollow" href="http://prisonplanet.com">prisonplanet.com</a> for example reveals 30 stories promoted to the front page of <a rel="nofollow" href="http://digg.com">Digg</a>. Conversely, <a rel="nofollow" href="http://www.digg.com/search?s=prisonplanet.com&submit=Search&section=news&type=url&area=promoted&sort=new">searching while excluding buried stories</a> reveals that only 16 of those stories haven&#8217;t been buried by the users. In addition, two stories were reported by the userbase as <a rel="nofollow" href="http://www.digg.com/politics/9_11_Toxic_Dust_Whistle_blower_Raided_By_SWAT_Team">possibly</a> <a rel="nofollow" href="http://www.digg.com/offbeat_news/Fox_News_Spin_Attack_Ends_With_Red-Faced_Anchors">inaccurate</a>. It even appears that prisonplanet has been [banned in the past only to be reinstated][3].

(NOTE: I don&#8217;t write this to single out prisonplanet.com, it was merely the first site I found that had a lot of buried stories. LGF or MichelleMalkin.com probably fit the bill as well)

This recent step back from the ideal of &#8220;disintermediation&#8221; has been subtle but the motivations for it are very cloudy. Can any of you think why they would do this?

 [1]: http://www.streetdirectory.com/travel_guide/2538/computers_and_the_internet/the_disintermediation_of_content.html
 [2]: http://www.themulife.com/?p=384
 [3]: http://www.digg.com/politics/Digg_bans_this_9_11_Truther_Next_British_Prime_Minister