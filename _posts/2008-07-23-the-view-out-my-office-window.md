---
title: The View Outside My Office Window
author: Ryan Underdown
excerpt: "I work at Falcon Field in Mesa, Arizona.  I stare out this window for hours every day.  I'm originally from Providence Rhode Island and you just don't see views like these.  "
layout: post
permalink: /digg/the-view-out-my-office-window.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
lead_image:
  - http://ryanunderdown.com/wp-content/uploads/2008/09/window.png
secondary_image:
  - http://ryanunderdown.com/wp-content/uploads/2008/09/windows.png
aktt_notify_twitter:
  - yes
categories:
  - digg
---
<a rel="nofollow" href="http://www.flickr.com/photos/r-u/2659405571/" title="photo sharing"><img src="http://farm4.static.flickr.com/3170/2659405571_8573b37885_m.jpg" alt="" style="border: solid 2px #E4F2FD;" /></a>  
  
<br clear="all" />

I (**edit** *used to* &#8211; our new offices are sweet David thanks!) work at Falcon Field in Mesa, Arizona. I stare out this window for hours every day. I&#8217;m originally from Providence Rhode Island and you just don&#8217;t see views like these. The land is so flat here that you can see hundreds of miles of skyline.