---
title: 'PHP &#038; MySQL Keyword Rankings Script'
author: Ryan Underdown
excerpt: "I've been working on a Google keyword rankings script that scrapes Google search engine results pages and stores the values in a mysql database.  This video shows it in action."
layout: post
permalink: /seo/keyword-rankings-script.php
aktt_notify_twitter:
  - yes
aktt_tweeted:
  - 1
categories:
  - scripting
  - SEO
tags:
  - Google
  - jquery
  - keyword rankings
  - mysql
  - php
  - scraper
  - scripting
  - serps
  - tools
---
I&#8217;ve been working on a Google keyword rankings script that scrapes Google search engine results pages and stores the values in a mysql database. The bulk of the heavy lifting is done via php &#038; cURL. I used some jquery to allow manual updating of rankings. Much of the scraper code was lifted from an old [5ubliminal][1] post that I can&#8217;t seem to find anymore. The following video is a short clip of a portion of the script in action. You can test out the script [here][2].

<center>
  <br />
</center>

<div style="margin-top:20px;margin-bottom:20px;border:1px solid #c0c0c0;vertical-align:bottom;text-align:center;">
  <h3 style="padding-top:20px">
    download
  </h3>
  
  <p>
    <a href="http://ryanunderdown.com/downloadkeywordrankings.zip">rank.zip</a></div> <h3>
      <strong>Files</strong>
    </h3>
    
    <ul>
      <li>
        <strong>connect.php</strong> &#8211; stores credentials for mysql connection
      </li>
      <li>
        <strong>add-domains.php</strong> &#8211; allows you to add a domain into the database. This domain is then presented as an option on the index page.
      </li>
      <li>
        <strong>add-keywords.php</strong> &#8211; checks a domain (already inserted via add-domains.php) against a keyword. If a match is found it inserts the data into the database.
      </li>
      <li>
        <strong>index.php</strong> &#8211; displays all of your domains
      </li>
      <li>
        <strong>final.php</strong> &#8211; the actual keyword rankings page. Allows you to update rankings and see each domain&#8217;s keyword rankings on one page.
      </li>
      <li>
        <strong>style.css</strong> &#8211; some css styling
      </li>
      <li>
        <strong>postjax2.php</strong> &#8211; processes serp results and stores values in the db
      </li>
      <li>
        <strong>keyword_rankings.sql</strong> &#8211; database scheme &#8211; load this query via phpmyadmin to setup your mysql db
      </li>
      <li>
        <strong>header.php</strong> &#8211; some navigation
      </li>
      <li>
        <strong>ajaxproxy.php</strong> &#8211; some cross domain proxy I found to allow you to get rankings from multiple ip addresses
      </li>
    </ul>
    
    <p>
      Google doesn&#8217;t particularly like it&#8217;s serps scraped &#8211; sometimes they will return garbage data if they suspect you are doing it. To get around this, we must bounce our requests off of multiple IP addresses. If you have a few cheap hosting accounts lying around, upload the <em>postajax.php</em> script to a folder accessible from the webserver. Insert your urls where these files can be found into the following section of code:
    </p>
    
    <pre class="brush: jscript; title: ; notranslate" title="">  
   var csurla = 'http://YOUR_URL_HERE/postjax2.php';
   var csurlb = 'http://YOUR_URL_HERE/postjax2.php';
   function cointoss(wot){
         var d= new Date().getSeconds();
         wot=(wot)? wot: [csurla, csurlb];
         d+= Math.round(Math.random()*10);
         return wot[d%2];
         }</pre>
    
    <p>
      The above example randomly chooses one of two urls to post the data to. This code could easily be changed to accommodate as many urls as you have access to. The next segment of script gathers the data from each table cell in a row of data. We then serialize the data and post it to to our ajax proxy which gets around the pesky cross domain ajax restriction. the $.Ajax command that is built in to jQuery then waits for the php proxy script to respond and then inserts the resulting data into the row we just clicked:
    </p>
    
    <pre class="brush: jscript; title: ; notranslate" title="">$(".update").click(function() {
       /* extract all the info out of the table cells */
       var selectR = $(this);
       var urlsearch = $(".urlse a").html();
       var se = $(selectR).find("td.se").html();
       var wt = $(selectR).find("td.wt").html();
       var str = $(selectR).find("td.keywords a").html();
       var ranking = $(selectR).find("td.rankingspan").html();
       var orank = $(selectR).find("td.orank").html();
       var delta = $(selectR).find("td.hclass div").html();
       var clicks = $('#tabler').find("td.clickstotal").html();
       var wttotal = $('#tabler').find("td.wttotal").html();
       var keysearch = str.replace('/%20', '+');
       var csurl = cointoss();
       var dataString = 'csurl=' + csurl + '&clicks=' + clicks + '&wttotal=' + wttotal + '&domain=' + urlsearch + '&keyword=' + keysearch + '&wt=' + wt + '&se=' + se + '&rank=' + ranking + '&orank=' + orank + '&delta=' + delta;
       $(this).html("&lt;td colspan='9' align='center'&gt;&lt;img src='http://PATH_TO/loading.gif'&gt;&lt;/td&gt;");

          $.ajax({
          type: "POST",
          url: "http://PATH_TO/ajaxproxy.php",
          data: dataString,
          cache: false,
          success: function(data){
          $(selectR).html(data);
          var totalr = 0;
          var tempint = 0;
          $("td.clicks").each(function(){
          if($(this).text().length &gt; 0){
           var tempint = parseFloat($(this).text()); 
           totalr += tempint;
           }})
          $("#clickstotal").html(totalr);

       $("td.wt").each(function(){
       if($(this).text().length &gt; 0){
           var tempwt = parseFloat($(this).text()); //convert to number
           totalwt += tempwt;
           totalwt = totalwt.toFixed(2);
           }})
          }
          });
      });</pre>

 [1]: http://blog.5ubliminal.com/
 [2]: http://seotrackr.com