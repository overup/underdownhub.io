---
title: Real Estate News Today
author: Ryan Underdown
excerpt: Real estate news from August 31, 2006.
layout: post
permalink: /real-estate/real-estate-news-today.php
aktt_notify_twitter:
  - yes
categories:
  - Finance
  - Real Estate
---
*   [&#8216;Exotic&#8217; mortgages seen losing their allure][1]
*   [London Surpasses NYC as Priciest Market][2]
*   [The Strongest and Weakest Housing Markets][3] 
*   [The HUD Hoax][4]

 [1]: http://www.msnbc.msn.com/id/14584569/
 [2]: http://ryanunderdown.com/2006/08/31/london-overtakes-nyc-as-worlds-priciest-market/
 [3]: http://ryanunderdown.com/2006/08/31/the-strongest-and-weakest-us-housing-markets-3/
 [4]: http://ryanunderdown.com/2006/08/31/the-hud-hoax/