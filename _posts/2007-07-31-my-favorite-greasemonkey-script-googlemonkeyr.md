---
title: 'My Favorite Greasemonkey Script: GoogleMonkeyR'
author: Ryan Underdown
layout: post
permalink: /greasemonkey/my-favorite-greasemonkey-script-googlemonkeyr.php
categories:
  - Greasemonkey
---
[GoogleMonkeyR][1] is a <a href="https://addons.mozilla.org/en-US/firefox/addon/748" target="_blank">greasemonkey</a> script written by a <a href="http://userscripts.org" target="_blank">userscripts.org</a> user named <a href="http://www.monkeyr.com/" target="_blank">mungushome</a> that I&#8217;ve fallen in love with lately. It alters the search results in google so you never need to click next page for more results and it is highly configurable allowing you to view the results in 1, 2 or 3 columns for easy access to as many search results as you can handle. It also removes the waste-of-page-space sponsored results that [google][2] loves to barrage you with. Surprisingly my favorite attribute of this script is the blue tint it adds to the back of each result which you can see here.

<p align="center">
  <a href="http://ryanunderdown.com/wp-content/uploads/2007/07/googlemonkeyr.gif" title="googlemonkeyr"><img src="http://ryanunderdown.com/wp-content/uploads/2007/07/googlemonkeyr.thumbnail.gif" alt="googlemonkeyr" /></a>
</p>

All of this is easily customizable and makes searching with google even that much easier. Definitely worth a download.

 [1]: http://userscripts.org/scripts/show/9310
 [2]: http://google.com