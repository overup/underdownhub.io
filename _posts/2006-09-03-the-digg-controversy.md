---
title: the digg controversy
author: Ryan Underdown
excerpt: 'The latest <a href="http://digg.com/software/Is_Digg_being_Rigged_More_data">rants</a> about how Digg has become an unfair system.  Stats from <a rel="nofollow" href="http://duggtrends.com/stats/entry/63.aspx">duggtrends</a> reveal that more stories are being buried than before the top users left.  My own <a rel="nofollow" href="http://digg.com/users/elebrio/submitted">weeks history</a> shows very little change from the norm. '
layout: post
permalink: /digg/the-digg-controversy.php
code_errorcode_worpress_seo:
  - 0
url_errorcode_worpress_seo:
  - http://www.wordpress-seo.com/redirection-par-defaut.php
aktt_notify_twitter:
  - yes
categories:
  - digg
  - SMO
---
The latest <a rel="nofollow" href="http://digg.com/software/Is_Digg_being_Rigged_More_data">rants</a> about how Digg has become an unfair system. Stats from <a rel="nofollow" href="http://duggtrends.com/stats/entry/63.aspx">duggtrends</a> reveal that more stories are being buried than before the top users left. My own <a rel="nofollow" href="http://digg.com/users/elebrio/submitted">weeks history</a> shows very little change from the norm. The <a rel="nofollow" href="http://www.wired.com/news/technology/0,71750-0.html?tw=wn_index_2">obligatory story</a> over at Wired. A <a rel="nofollow" href="http://www.searchzone.exofire.net/chat/digg/">chat room</a> for you to sound off. Digg.com userbase accused of<a rel="nofollow" href="http://www.shelleytherepublican.com/2006/04/27/diggcom-theinquirernet-gutter-dwelling-liberal-attack-sites.aspx"> illegal left-wing vigilantism.</a> Digg now runs on your [DS][1], <a rel="nofollow" href="http://wavebroadcast.com/2006/09/08/official-digg-compatible-site-for-mobile-devices/">your phone or pda</a>

 [1]: http://www.flickr.com/photos/milkandmonsters/234852849/