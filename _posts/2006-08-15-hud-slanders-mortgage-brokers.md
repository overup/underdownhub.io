---
title: 'National Association of Realtors: biggest lobbying group in all of DC'
author: Ryan Underdown
excerpt: "I came across an interesting piece on the Department of Housing and Urban Development's website.  Apparently the bureaucrats in HUD have no idea how the mortgage process works and are completely unaware of the DOJ pressing their anti-trust case against the NAR.  "
layout: post
permalink: /real-estate/hud-slanders-mortgage-brokers.php
wjt_diPostTopic:
  - business_finacnce
DiggUrl:
  - http://digg.com/business_finance/the_biggest_lobbying_group_in_washington_dc_it_s_not_the_nra
DiggData:
  - href=http://digg.com/business_finance/the_biggest_lobbying_group_in_washington_dc_it_s_not_the_nra
aktt_notify_twitter:
  - yes
categories:
  - Politics
  - Real Estate
---
I came across an interesting piece on the Department of Housing and Urban Development&#8217;s website. Apparently the bureaucrats in HUD have no idea how the mortgage process works and are completely unaware of the DOJ pressing their anti-trust case against the NAR. From their publication entitled [Looking for the best mortgage][1] they state:

> Whether you are dealing with a lender or a broker may not always be clear. Some financial institutions operate as both lenders and brokers. And most brokersâ€™ advertisements do not use the word â€œbroker.â€ Therefore, be sure to ask whether a broker is involved. This information is important because brokers are usually paid a fee for their services that may be separate from  
> and in addition to the lenderâ€™s origination or other fees. A brokerâ€™s compensation may be in the form of â€œpointsâ€ paid at closing or as an add-on to your interest rate, or both. You should ask each broker you work with how he or she will be compensated so that you can compare the different fees. 

But, the mortgage broker has to be making their money somehow right? You might reasonably believe HUD when it tells you that the cost has to be passed on to the customer right? Wrong. This myth has propagated from the fact that while both direct lenders and mortgage brokers are governed by the same laws, there is one distinction. A posting on [Wikipedia][2] might help clear up the confusion:

> The difference between the &#8220;Broker&#8221; and &#8220;Banker&#8221; is the banker&#8217;s ability to use a short term credit line (known as a warehouse line) to fund the loan until they can sell the loan to the secondary market. Then, they repay their warehouse lender and obtain a profit on the sale of the loan. The borrower will often get a letter notifying them their lender has sold or transferred the loan.
> 
> Brokers must also disclose Yield spread premium while Bankers do not. This has created an ambiguous and difficult identification of the true cost to obtain a mortgage. The stricter Broker disclosure requirements, especially the Good Faith Estimate, can often create the illusion that they are charging more to obtain the exact same mortgage when compared to a Banker, when in fact they may cost the same or the Brokers offer may even be less costly. 

Anyone that has worked on both sides of the lending business understands that both retail lenders and brokers, who sell their loans to the wholesale divisions of those same direct lenders, are regulated by Section 32 of RESPA governing acceptable fees charged to the customer. These fees combined cannot exceed 6% of the total transaction. Real estate agents on the other hand regularly charge 6% of total fees on EVERY transaction yet they are given a rousing endorsement on their website [here][3]

> Choosing the right person to sell your home is one of the most important steps of selling.

Could this have anything to do with the NAR being the number one PAC in terms of campaign contributions? They come in even higher than the trial lawyers according to [OpenSecrets.org][4]

Now why would a mortgage broker be held to such stringent laws while direct lenders are given a pass? Let&#8217;s follow the money. On [OpenSecrets.org][5] we find that the commercial banking lobby ranks 9th among all lobby groups in total donations to political candidates.

<img src="http://ryanunderdown.com/wp-content/uploads/2006/08/lobbyists.gif" alt="Commercial Banking Campaign Contributions" width="400" />

Commercial Banking interests contributed $30 million in the 2004 election cycle. The National Association of Mortgage Brokers reports that mortgage brokers have only [contributed $1.9 million since 1999. ][6]

 [1]: http://www.hud.gov/buying/booklet.pdf
 [2]: http://en.wikipedia.org/wiki/Mortgage_broker#Industry_competitiveness
 [3]: http://www.hud.gov/selling/intrview.cfm
 [4]: http://www.opensecrets.org/pacs/topacs.asp
 [5]: http://www.opensecrets.org/industries/indus.asp?Ind=F03
 [6]: http://www.campaignmoney.com/Mortgage_Broker.asp?pg=14