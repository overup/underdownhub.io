---
title: Big Unit Questionable for Playoffs
author: Ryan Underdown
excerpt: 'Yankees lefty Randy Johnson has a herniated disc in his lower back, an injury that could prevent him from pitching in the playoffs next week.  The injury was discovered during an MRI exam Thursday. After being examined by team physician Dr. Stuart Hershon and back specialist Dr. Paul Kuflick, Johnson received an epidural injection Friday.'
layout: post
permalink: /digg/big-unit-questionable-for-playoffs.php
aktt_notify_twitter:
  - yes
categories:
  - digg
---
Yankees lefty Randy Johnson has a herniated disc in his lower back, an injury that could prevent him from pitching in the playoffs next week. The injury was discovered during an MRI exam Thursday. After being examined by team physician Dr. Stuart Hershon and back specialist Dr. Paul Kuflick, Johnson received an epidural injection Friday.

[read more][1]&nbsp;|&nbsp;[digg story][2]

 [1]: http://www.breitbart.com/news/2006/09/29/D8KEQBHO0.html
 [2]: http://digg.com/baseball/Big_Unit_Questionable_for_Playoffs