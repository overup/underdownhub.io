---
title: Interest Rate Watch
author: Ryan Underdown
excerpt: |
  |
    Friday's nonfarm payrolls report for August, with its data on job growth and wage inflation, is expected to be an important influence on the Fed and its next decision on short-term U.S. interest rates, on Sept. 20.
layout: post
permalink: /business/interest-rate-watch.php
aktt_notify_twitter:
  - yes
categories:
  - Business
---
## Upcoming Economic Indicators

<hr width="50%" align="left" />

Friday&#8217;s nonfarm payrolls report for August, with its data on job growth and wage inflation, is expected to be an important influence on the Fed and its next decision on short-term U.S. interest rates, on Sept. 20.  


## July Data

<hr width="50%" align="left" />
Change in Payroll Employment:
  
History +113,000(p) in Jul 2006</p> 
Change in Average Hourly Earnings:  
History +$0.07(p) in Jul 2006

Change in Average Weekly Hours:  
History unchanged in Jul 2006

Change in Manufacturing Average Weekly Hours:  
History +0.2(p) in Jul 2006

Change in Aggregate Hours Index:  
History +0.1(p) in Jul 2006

Change in Real Earnings:  
History -$0.01(p) in Jul 2006

## August Expectations

<hr width="50%" align="left" />
A rise of 120,000 in non-farm payrolls is the central market expectation for August.
  
  
Get it [here][1] on friday.</p>

 [1]: http://www.bls.gov/ces/