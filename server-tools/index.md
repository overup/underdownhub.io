---
title: Server Tools
author: Ryan Underdown
layout: page
---
<div class="twocolumn" style="width:48%;float:left">
  <h5>
    <a href="http://aws.amazon.com" title="Amazon Web Services">Amazon Web Services</a>
  </h5>
  
  <p>
    The easiest way to scale your website/app
  </p>
  
  <h5>
    <a href="http://newrelic.com" title="New Relic" target="_blank">New Relic</a>
  </h5>
  
  <p>
    Monitor your entire stack &#8211; php, python, mysql, more
  </p>
  
  <h5>
    <a href="http://www.webpagetest.org">WebPageTest.org</a>
  </h5>
  
  <p>
    An amazing, free web performance testing tool
  </p>
  
  <h5>
    <a href="http://pingdom.com" title="Pingdom" target="_blank">Pingdom</a>
  </h5>
  
  <p>
    Server uptime/latency monitoring
  </p>
  
  <h5>
    <a href="http://mon.itor.us" title="Mon.itor.us" target="_blank">Monitor.us</a>
  </h5>
  
  <p>
    Server uptime/latency monitoring
  </p>
  
  <h5>
    <a href="http://blitz.io" title="Blitz.io" target="_blank">Blitz.io</a>
  </h5>
  
  <p>
    Load testing tool
  </p>
  
  <h5>
    Memcached
  </h5>
  
  <p>
    <a href="http://www.mysqlperformanceblog.com/2008/11/26/a-quick-way-to-get-memcached-status/">&#8220;top&#8221; for memcached</a> </div> <div style="width:48%;float:right">
      <h5>
        <a href="http://httpd.apache.org/docs/" target="_blank">Apache</a> &#8211; industry standard http server
      </h5>
      
      <p>
        <a href="http://code.google.com/p/mod-spdy/wiki/GettingStarted">mod_spdy</a>, <a href="http://code.google.com/p/modpagespeed/">mod_pagespeed</a>, <a href="http://www.cheatography.com/davechild/cheat-sheets/mod-rewrite/">mod rewrite cheatsheet</a>, <a href="http://serverfault.com/questions/45042/which-to-install-apache-worker-or-prefork-what-are-the-dis-advantages-of-eac">worker vs prefork</a>
      </p>
      
      <h5>
        <a href="https://www.varnish-cache.org/" target="_blank">Varnish</a> &#8211; reverse caching proxy
      </h5>
      
      <p>
        Some useful VCLs: <a href="https://www.varnish-cache.org/trac/wiki/VCLExampleHitMissHeader">set hit/miss header</a>, <a href="https://www.varnish-cache.org/trac/wiki/VCLExampleStripGoogleAdwordsGclidParameter">cache adwords traffic</a>, <a href="https://www.varnish-cache.org/trac/wiki/VCLExampleAvoidHotlinking">prevent hotlinking</a>, <a href="https://www.varnish-cache.org/trac/wiki/VCLExampleSimpleRestarts">serve old cache on 5xx errors</a>, <a href="http://omninoggin.com/web-development/block-unwanted-spam-bots-using-varnish-vcl/">block malicious bots</a>
      </p>
      
      <h5>
        Some useful tutorials
      </h5>
      
      <p>
        <a href="https://www.digitalocean.com/community/articles/how-to-protect-ssh-with-fail2ban-on-ubuntu-12-04">Setup fail2ban to block malicious ssh login attempts</a>
      </p>
      
      <h5>
        <a href="http://wiki.nginx.org/Main" target="_blank">nginx</a>
      </h5>
      
      <p>
        High concurrency webserver, also very useful as a reverse caching proxy.
      </p>
      
      <h5>
        <a href="http://www.cloudflare.com" target="_blank">Cloudflare</a>
      </h5>
      
      <p>
        Tough to summarize, hosted dns, reverse caching proxy with anti DoS features
      </p>
      
      <h5>
        <a href="https://papertrailapp.com">Paper Trail</a>
      </h5>
      
      <p>
        convenient logging of system info
      </p>