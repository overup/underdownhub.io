<html>

<head>
  <title>Keyword Generation Script</title>
  <style type="text/css">
  body * {
    font-family: arial;
    font-size: 12px;
  }
  .checkbox {
    padding: 0px;
    margin: 0px;
    margin-right: 5px;
  }
  </style>
</head>

<body>

<p align="center"><img src="http://indexedcontent.com/wp-content/uploads/2008/01/iclogo.png" alt="indexed content" /></p>
<p align="center">Enter keywords or modifiers in each box. Separate each word or phrase with a comma. Press generate to get results.</p>
<?php



/*
============================================================================================================
        Keyword Phrase List Generator
============================================================================================================

        Author               : Mike Thomas

        Version              : 1.0

        Description          : Free Open Source Keyword Phrase List Generator

        Copyright            : (c) SeoBook.com, licensed under the GPL ( http://www.gnu.org/licenses/gpl.txt )

        History              : 11-10-05
                               - Initial Write

        Function             : Tool allows webmasters to enter a list of keyword terms and modifiers to 
		                       generate a list of keyword phrase to use for bidding on pay per click search
							   engine marketing campaigns.
============================================================================================================
*/

$keywords = array();
$wordlists = array();

for ($x = 1; $x <= 5; $x++) {
  if (!empty($_POST['box' . $x])) {
    array_push($wordlists,explode(',',$_POST['box' . $x]));
  }
}

$keywords = array();
if ((count($wordlists[0]) > 0) && (count($wordlists[1]) > 0)) {
  foreach ($wordlists[0] AS $word1) {
    foreach ($wordlists[1] AS $word2) {
      $temp = $word1 . " " . $word2;
      if (count($wordlists) == 2) array_push($keywords, $temp);
      else {
        foreach ($wordlists[2] AS $word3) {
          $temp = $word1 . " " . $word2 . " " . $word3;
          if (count($wordlists) == 3) array_push($keywords, $temp);
          else {
            foreach ($wordlists[3] AS $word4) {
              $temp = $word1 . " " . $word2 . " " . $word3 . " " . $word4;
              if (count($wordlists) == 4) array_push($keywords, $temp);
              else {
                foreach ($wordlists[4] AS $word5) {
                  $temp = $word1 . " " . $word2 . " " . $word3 . " " . $word4 . " " . $word5;
                  if (count($wordlists) == 5) array_push($keywords, $temp);
                }
              }
            }
          }
        }
      }
    }
  }
}
function show_box($num) {
  if ((empty($_POST['box'.$num])) && (!empty($_POST['holder' . $num]))) {
    $contents = $_POST['holder'.$num];
    $disabled = " disabled";
  } else {
    $contents = $_POST['box'.$num];
    $disabled = "";
  }
  echo "<strong>Word List $num:</strong>\n";
  echo "<div style=\"margin-bottom: 15px;\">\n";
  echo "  <textarea id=\"box$num\" name=\"box$num\" style=\"height: 100px;\" rows=\"5\" cols=\"60\" wrap=\"on\"$disabled>$contents</textarea><br>\n";
  echo "  <a href=\"#\" onClick=\"move($num,'up')\">move up</a> | <a href=\"#\" onClick=\"move($num,'down')\">move down</a> | <a href=\"#\" onClick=\"toggle($num)\">toggle</a>\n";
  echo "  <input id=\"holder$num\" name=\"holder$num\" type=\"hidden\" value=\"" . $_POST['holder' . $num] . "\">\n";
  echo "</div>\n";
}
?>

<script type="text/javascript">
function move(num,direction) {
  if (direction == 'up') {
    var newnum = num--;
  } else if (direction == 'down') {
    var newnum = num++;

  }
  if ((newnum >= 1) && (newnum <= 5)) {
    var temp = document.getElementById('box'+newnum).value;
    document.getElementById('box'+newnum).value = document.getElementById('box'+num).value;
    document.getElementById('box'+num).value = temp;
  }
}
function store() {
  if (document.getElementById('store1').value == '') {
    document.getElementById('store1').value = document.getElementById('results').value;
  } else if (document.getElementById('store2').value == '') {
    document.getElementById('store2').value = document.getElementById('results').value;
  } else if (document.getElementById('store3').value == '') {
    document.getElementById('store3').value = document.getElementById('results').value;
  } else {
    document.getElementById('store3').value = document.getElementById('store2').value;
    document.getElementById('store2').value = document.getElementById('store1').value;
    document.getElementById('store1').value = document.getElementById('results').value;
  }
  document.getElementById('results').value = '';
}
function combine() {
  document.getElementById('results').value += document.getElementById('store1').value + document.getElementById('store2').value + document.getElementById('store3').value;
  document.getElementById('store3').value = '';
  document.getElementById('store2').value = '';
  document.getElementById('store1').value = '';
}
function toggle(num) {
  var box = document.getElementById('box'+num);
  var holder = document.getElementById('holder'+num);
  if (box.disabled == true) {
    holder.value = '';
    box.disabled = false;
  } else {
    holder.value = box.value;
    box.disabled = true;
  }
}
function csv() {
  document.getElementById('csv').value = document.getElementById('results').value;
  document.getElementById('csvform').submit();
}
</script>
<div style="width:10px;">&nbsp;</div>
<div style="width:760:px;margin:0 auto;">
<form style="text-align:center;" name="csvform" id="csvform" action="gencsv.php" method="post">
  <input name="content" id="csv" type="hidden" value="">
</form>

<form style="text-align:center;" name="generator" action="generator.php" method="post">



<table border="0" cellspacing="5px" align="center">
  <tr><td colspan="2" align="center">

    <strong>URL:</strong>
    <input name="url" type="text" value="<?php echo $_POST['url']; ?>" style="width: 200px;"> <span style="font-size: 10px;">(ex. http://www.site.com)</span>
    &nbsp;
    <strong>Bid:</strong>
    <input name="bid" type="text" value="<?php echo $_POST['bid']; ?>" style="width: 50px;"> <span style="font-size: 10px;">(ex. 0.25)</span>
    </td></tr>
    <tr><td colspan="2" align="center">

    Broad: <input class="checkbox" name="broad" type="checkbox" value="on" <?php if (($_POST['broad'] == "on") || (!isset($_POST['broad']))) echo "checked"; ?>>
    Phrase: <input class="checkbox" name="phrase" type="checkbox" value="on" <?php if ($_POST['phrase'] == "on") echo "checked"; ?>>
    Exact: <input class="checkbox" name="exact" type="checkbox" value="on" <?php if ($_POST['exact'] == "on") echo "checked"; ?>>

    <input type="submit" value="Generate">

   </td></tr>
  <tr>
    <td valign="top" style="border: 1px solid #CCCCFF; padding: 5px; background-color: #FCFCFF">
      <?php
      for ($x = 1; $x <= 5; $x++) {
        show_box($x);
      }
      ?>

    </td>
    <td valign="top" style="border: 1px solid #CCCCCC; padding: 5px; background-color: #FCFCFC">
      <strong>Current Results (click to select all):</strong><br>
      <div style="margin-bottom: 17px;">
      <textarea id="results" name="results" rows=14 cols=60 wrap="off" style="height: 238px; overflow-x: hidden; overflow-y: scroll;" onClick="this.select()"><?php
      foreach ($keywords AS $keyword) {
        $bid = '';
        $url = '';
        $count = 0;
        if (!empty($_POST['bid'])) {
          $bid = " ** " . $_POST['bid'];
        }
        if (!empty($_POST['url'])) {
          $url = " ** " . $_POST['url'];
        }
        if ($_POST['broad'] == "on") {
          echo trim($keyword) . $bid . $url . "\n";
          $count++;
        }
        if ($_POST['phrase'] == "on") {
          echo "\"" . trim($keyword) . "\"" . $bid . $url . "\n";
          $count++;
        }
        if ($_POST['exact'] == "on") {
          echo "[" . trim($keyword) . "]" . $bid . $url . "\n";
          $count++;
        }
      }
      ?></textarea>
        <div>
          <input type="button" value="Store Results" onClick="store()">
          <input type="button" value="Combine Results" onClick="combine()">
          <input type="button" value="CSV" onClick="csv()">
        </div>
      </div>
      <strong>Stored Results 1:</strong>
      <div style="margin-bottom: 15px;">
        <textarea id="store1" name="store1" rows="5" cols="60" wrap="off" style="height: 100px; overflow-x: hidden; overflow-y: scroll;"><?php echo stripslashes($_POST['store1']); ?></textarea><br>
        <a href="#" onClick="document.getElementById('store1').value=''">clear</a>
      </div>
      <strong>Stored Results 2:</strong>
      <div style="margin-bottom: 15px;">
        <textarea id="store2" name="store2" rows="5" cols="60" wrap="off" style="height: 100px; overflow-x: hidden; overflow-y: scroll;"><?php echo stripslashes($_POST['store2']); ?></textarea><br>
        <a href="#" onClick="document.getElementById('store2').value=''">clear</a>
      </div>
      <strong>Stored Results 3:</strong>
      <div style="margin-bottom: 15px;">
        <textarea id="store3" name="store3" rows="5" cols="60" wrap="off" style="height: 100px; overflow-x: hidden; overflow-y: scroll;"><?php echo stripslashes($_POST['store3']); ?></textarea><br>
        <a href="#" onClick="document.getElementById('store3').value=''">clear</a>
      </div>

    </td>
  </tr>
</table>
</form>

</body>
</html>